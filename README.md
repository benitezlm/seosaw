# SEOSAW - A Socio-Ecological Observatory for Southern African Woodlands

This repository holds data and code related to the SEOSAW project. Here is a breakdown of the repository structure

* `cleaning` - Site level R scripts to transform datasets into SEOSAW-formatted data.
* `seosawr` - The SEOSAW R package, containing functions used to clean and manipulate SEOSAW-formatted data
* `doc` - Manuals, resources, code and outputs for the SEOSAW website

Note that the SEOSAW dataset resides in a different repository, with more limited access.

## How do I get set up?

In a UNIX terminal, `git clone` this repository, or alternatively download it as a zip file:

```
git clone git@bitbucket.org:miombo/seosaw.git
```

## Repo etiquette

* Try to follow the guidelines here: [Good Enough Practices for Scientific Computing](https://swcarpentry.github.io/good-enough-practices-in-scientific-computing/)
* Please keep all code and outputs for specific analyses in separate directories found within `analysis/`.
* Place thorough explanatory comment at the start of every program, no matter how short the program. 
* Aim to make all programs automated and free of errors, to increase reproducibility of research.
* Refrain from running `git add -A`, only stage and commit source files, not intermediate files or large outputs.
* Do not commit SEOSAW data to this repository. The SEOSAW data is sensitive and should be kept in a separate repository.

## SEOSAW R package

The SEOSAW R package provides functions and example workflows for creating and cleaning SEOSAW-like data, and for analysing the existing SEOSAW dataset.

## Contact details

Please contact the repository owner: Dr. Casey M. Ryan - casey.ryan@ed.ac.uk
