# Stem table checking functions

#' Flag odd looking diameter:height ratio values
#'
#' Uses a linear model to flag diameter:height ratios that are clearly unusual
#'
#' @param x dataframe containing stem measurements
#' @param diam column name of stem diameter
#' @param height column name of stem height measurements
#' @param ... parameters passed to \code{relCheck}
#'
#' @return Vector of row indices showing flagged diameter:height
#'     ratio values, and optionally a plot of the diameter:height allometry
#'
#' @keywords internal
#' @noRd
#'
diamHeightCheck <- function(x, diam = "diam", height = "height", ...) {
  relCheck(x, diam, height, 
    warn = "Diameter:height ratio outside sigma", ...)
}

#' Are death values consistent and logical?
#'
#' @param x dataframe containing stem emasurements
#' @param alive column name string of alive status
#' @param stem_status column name string of stem status
#' @param broken_per_remain column name string of broken percentage remaining
#' @param broken_height column name string of broken height
#' @param stem_mode column name string of stem mode
#' @param damage_cause column name string of damage cause
#' @param damage_cause_human column name string of human damage cause
#' @param decay column name string of stem decay
#' @param output logical; if TRUE a vector of row indices is returned, 
#'     if FALSE only a warning
#'
#' @return Vector of row indices which show logical
#'     inconsistencies in the death classification of stems
#'
#' @keywords internal
#' @noRd
#'
deathCheck <- function(x, alive = "alive", stem_status = "stem_status", 
  broken_per_remain = "broken_per_remain", broken_height = "broken_height",
  stem_mode = "stem_mode", damage_cause = "damage_cause", 
  damage_cause_human = "damage_cause_human", decay = "decay",
  output = FALSE) {

  death_list <- list()

  # Alive but containing death information
  death_list[[1]] <- which(
    ((x[[alive]] == "a" & !is.na(x[[stem_status]])) |
      (x[[alive]] == "a" & !is.na(x[[stem_mode]])) |
      (x[[alive]] == "a" & !is.na(x[[damage_cause]])) |
      (x[[alive]] == "a" & !is.na(x[[damage_cause_human]])) |
      (x[[alive]] == "a" & !is.na(x[[decay]]))
    )
  )

  # Dead but missing death information
  death_list[[2]] <- which(
    ((x[[alive]] == "d" & is.na(x[[stem_status]])) |
      (x[[alive]] == "d" & is.na(x[[stem_mode]])) |
      (x[[alive]] == "d" & is.na(x[[damage_cause]])) |
      (x[[alive]] == "d" & is.na(x[[decay]]))
    )
  )

  # Human death but not specific cause
  death_list[[3]] <- which(
    (x[[damage_cause]] == "h" & is.na(x[[damage_cause_human]]))
  )

  # Not human damage but human cause given
  death_list[[4]] <- which(
    (x[[damage_cause]] != "h" & !is.na(x[[damage_cause_human]]))
  )

  # Death mode snapped but no broken height or percentage remaining
  death_list[[5]] <- which(
    ((x[[stem_mode]] == "p" & is.na(x[[broken_per_remain]])) |
      (x[[stem_mode]] == "p" & is.na(x[[broken_height]]))
    )
  )

  names(death_list) <- c(
    "Alive but contains death information",
    "Dead but missing death information",
    "Human death but no human cause",
    "Not human damage but human cause given",
    "Death mode is snapped but no broken height or percentage remaining")

  bad_list <- lapply(1:length(death_list), function(y) {
    warnFormat(death_list[[y]], names(death_list)[y], "warning", 15)
    death_list[[y]]
  })

  if (output) {
    if (length(bad_list[unlist(lapply(bad_list, function(y) length(y) > 0))]) > 0) {
    return(bad_list[unlist(lapply(bad_list, function(y) length(y) > 0))])
    }
  }
}

#' Is the plot ID truly unique within the SEOSAW dataset
#'
#' @param x dataframe of stem measurements
#' @param plot_id column name string of plot ID
#' @param data_plot_id vector of plot_id values from the larger SEOSAW dataset
#' @param output logical; if TRUE values of plot_id which already exist 
#'     in SEOSAW dataset are returned
#'
#' @return Vector of \code{plot_id} values which are not unique within the 
#'     larger SEOSAW dataset
#'
#' @keywords internal
#' @noRd
#'
plotIDUnique <- function(x, plot_id = "plot_id", data_plot_id, 
  output = FALSE) {
  if (any(x[[plot_id]] %in% data_plot_id)) {
    warnFormat(x[[plot_id]][x[[plot_id]] %in% data_plot_id],
      "Plot IDs already taken:", "warning", 15)
  }

  if (output) {
    return(x[x[[plot_id]] %in% data_plot_id, plot_id])
  }
}

#' Are measurement IDs unique?
#'
#' @param x dataframe of stem measurements
#' @param measurement_id column name string of measurement IDs
#' @param output logical; if TRUE a vector of row indices is returned, 
#'     if FALSE only a warning
#'
#' @return Vector of row indices showing flagged diameter values
#' 
#' @keywords internal
#' @noRd
#'
measurementIDUnique <- function(x, measurement_id = "measurement_id", 
  output = FALSE) {

  dups <- which(duplicated(x[[measurement_id]]))

  warnFormat(dups, "Duplicated measurement IDs:", "warning", 15)

  if (output) {
    return(dups)
  }
}

#' Identify diameter values which are unusually common 
#' 
#' @param x dataframe of stem measurements
#' @param diam column name string of stem diameters
#' @param val either a single integer threshold stem diameter above which values 
#'     are likely to be present due to missed decimals, or a vector of diameter
#'     values where rounding may have occurred
#' @param sigma number of standard deviations in residuals to act as threshold
#'     to define outliers
#' @param output logical; if TRUE a vector of row indices is returned, 
#'     if FALSE only a warning
#'
#' @return Vector of row indices showing flagged diameter values
#'
#' @keywords internal
#' @noRd
#'
diamDecCheck <- function(x, diam = "diam", val = 50, sigma = 2, 
  output = FALSE) {

  if (is.null(val)) {
    stop("Either threshold diameter or vector of diameter values must be set")
  } 

  # Generate breaks for bins
  breaks <- seq(
    round(min(x[[diam]], na.rm = TRUE), 0),
    max(x[[diam]], na.rm = TRUE),
    0.1)

  # Create binned dataframe
  bin <- as.data.frame(table(cut(x[[diam]], breaks)))
  bin$bin_rank <- as.numeric(gsub(".*,", "", gsub("]", "", bin$Var1)))

  # Remove empty bins
  bin <- bin[bin$Freq > 0,]

  # Linear model of diameter class distribution
  mod <- lm(log(Freq + 1) ~ bin_rank, data = bin)

  # Model residuals
  mod_resid <- resid(mod)

  # Extract standard deviation of residuals (sigma)
  mod_sigma <- sigma(mod)
    
  # Binned diameters with residuals larger than acceptable sigma
  index <- unname(
    which(
      abs(mod_resid - mean(mod_resid, na.rm = TRUE)) >= (mod_sigma * sigma)
    )
  )

  if (length(val) == 1) {
    # Clean up flagged bins
    bin_flag <- bin[index,]
    bin_flag <- bin_flag[bin_flag$bin_rank >= val, "bin_rank"]

    warnFormat(bin_flag, "Unusually abundant diameter values:", "warning", 15)
  } else if (length(val) > 1) {
    # Integer diameters with residuals larger than acceptable
    bin_flag <- bin[index,]
    bin_flag <- bin_flag[bin_flag$bin_rank %in% val, "bin_rank"]

    warnFormat(bin_flag, "Over-represented diameter values:", "warning", 15)
  }

  if (output) {
    return(bin_flag) 
  }
}

#' Run a number of table-level data quality checks on stem-level data
#'
#' @param x dataframe containing stem level measurements
#' @param funs named list of functions to check stem level data
#' @param ... additional arguments passed to functions listed in \code{funs}
#'
#' @return List of row indices flagged by each of functions in \code{funs}
#'
#' @export
#'
stemTableCheck <- function(x,
  funs = list("diamHeightCheck" = diamHeightCheck, 
    "deathCheck" = deathCheck,
    "diamDecCheck" = diamDecCheck, 
    "measurementIDUnique" = measurementIDUnique), ...) {
  lapply(1:length(funs), function(y) {
    message("Running:", names(funs[y]))
    funs[[y]](x, ...)
  })
}

