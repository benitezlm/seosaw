#' Extract the modal value of a vector
#'
#' @param x vector
#'
#' @return string of the most frequent value in x
#' 
#' @keywords internal
#' @noRd
#' 
Mode <- function(x, ...) {
  x <- x[!is.na(x)]
  ux <- unique(x)
  ux[which.max(tabulate(match(x, ux)))]
}

#' Catch non-standard values and return NAs
#'
#' @param x vector 
#' @param zero logical, should zeroes also be converted to NA? \code{FALSE} by default
#'
#' @return vector
#' 
#' @examples 
#' fillNA(c(1,2,3,Inf,NA,NaN,"nan","dinf","Inf","  ", " ff    "))
#' fillNA(c(0,1,2), zero = TRUE)
#'
#' @keywords internal
#' @noRd
#' 
fillNA <- function(x, zero = FALSE) {
  if (is.factor(x)) { x <- as.character(x) }
  out <- unlist(lapply(split(x, seq_along(x)), function(y) {
      # Inf, NaN, NA
      if (is.na(y) | is.nan(y) | is.infinite(y)) { 
        return(NA_character_) 
      # Character representations of NA, NaN, Inf,
      } else if (grepl("^n/a$|^na$|^nan$|^inf$", 
          trimws(iconv(y, "latin1", "ASCII", sub = " ")), 
          ignore.case = TRUE)) {
        return(NA_character_)
      # Empty character strings
      } else if (is.character(y) & nchar(trimws(y)) == 0) {
        return(NA_character_)
      } else {
        if (zero == TRUE) {
          if (y == 0) {
            return(NA)
          } else {
            return(y)
          }
        }
        return(y)
      }
      # Optionally change zeroes to NA
  }))
  # Change class to original
  out <- unname(as(out, class(x)))
  return(out)
}

#' Check if contains NA with message
#'
#' @param x vector 
#' @param warn logical; return warning instead of error
#' @param colname optional column name string to display in error/warning 
#'
#' @return Vector of class "character"
#' @keywords internal
#' @noRd
#'
naCatch <- function(x, warn = FALSE, colname = NULL) {
  if (is.null(colname)) {
    colname <- tryCatch( { 
      deparse(sys.calls()[[sys.nframe()-1]])
    },
      error=function() { 
        return(paste("")) 
      }
    )
  }
  if (anyNA(x)) {
    num <- length(x[is.na(x)])
    den <- length(x)
    output <- paste0(colname, ":", num, "/", den, " NAs")
    if (warn) {
      warning(output, call. = FALSE)
    } else {
      stop(output)
    }
  }
}

#' Check if vector contains negative values with message
#'
#' @param x vector 
#' @param warn logical; return warning instead of error
#' @param zero logical; if TRUE, also disallow zero
#' @param colname optional column name string to display in error/warning 
#'
#' @return Vector of class "character"
#' @keywords internal
#' @noRd
#'
negCatch <- function(x, warn = FALSE, zero = FALSE, colname = NULL) {
  if (is.null(colname)) {
    colname <- tryCatch( { 
      deparse(sys.calls()[[sys.nframe()-1]])
    },
      error=function() { 
        return(paste("")) 
      }
    )
  }

  if (zero) {
    testfunc <- `<=`
  } else { 
    testfunc <- `<`
  }

  x_nona <- x[!is.na(x)]
  if (any(testfunc(x_nona, 0))) {
    num <- length(which(testfunc(x_nona, 0)))
    den <- length(x)
    output <- paste0(colname, ":", num, "/", den, " negative values")
    if (warn) {
      warning(output)
    } else {
      stop(output)
    }
  }
}

#' Construct a generic factor with checks
#'
#' @param x vector
#' @param factor_levels character string referencing entry in \code{factor_levels()}
#' @param colname optional column name string to display in error/warning 
#' @param ... optional arguments passed to \code{coerceCatch()}
#'
#' @return Vector of class "factor"
#' @keywords internal
#' @noRd
#'
genericFactor <- function(x, factor_levels, colname = NULL, ...) {
  if (is.null(colname)) {
    colname <- tryCatch( { 
      paste("of type", deparse(sys.calls()[[sys.nframe()-1]])) 
    },
      error = function() { 
        return(paste("")) 
      }
    )
  } else {
    colname <- paste("in", colname)
  }
  x <- fillNA(x)
  x <- coerceCatch(x, as.factor, ...)
  factor_levels <- factor_levels()[[factor_levels]]
  output <- paste("Illegal factor level", colname)

  if (!all(levels(x) %in% factor_levels, x %in% factor_levels)) {
    stop(paste(output, ":",
      paste(unique(x)[!unique(x) %in% factor_levels], collapse = ", "),
      "\n\t", "allowed levels:", paste(factor_levels, collapse = ", ")))
  }
  structure(x, class = "factor")
}

#' Catch NA coercion errors
#'
#' @param x vector
#' @param fun coercion function, e.g. \code{as.integer}
#' @param colname optional column name string to display in error/warning 
#' 
#' @return Vector coerced by fun 
#' @keywords internal
#' @noRd
#' 
coerceCatch <- function(x, fun, colname = NULL) { 
  if (is.null(colname)) {
    colname <- tryCatch( { 
      paste("of type", deparse(sys.calls()[[sys.nframe()-1]])) 
    },
      error=function() { 
        return(paste("")) 
      }
    )
  }

  output <- paste("Unable to coerce column", colname, "to", deparse(substitute(fun)))

  tryCatch({
    fun(x)
  }, warning = function(w) {
    stop(paste0("converted from warning: ", output, ": ", conditionMessage(w)))
  })
}

#' Format warnings, messages and errors
#'
#' @param x vector of row positions, IDs etc. to display
#' @param warn character string with warning, message or error
#' @param type choose either "warning", "message" or "error" 
#' @param n integer, number of items from x to return in warning
#' @keywords internal
#' @noRd
#' 
warnFormat <- function(x, warn, type, n = 10, sep = ",") {
  if (length(x) == 0) {
    return(invisible())
  }

  nlen <- ifelse(length(x) > n, n, length(x))
  endv <- ifelse(length(x) > n, " ...", "")
  warn <- paste0(trimws(warn), " ")

  if (length(type) != 1 | !type %in% c("warning", "message", "error")) {
    stop("warn not 'warning', 'message' or 'error'")
  }

  if (length(x) > 0) {
    if (type == "warning") {
      warning(warn, "\n", paste(x[1:nlen], collapse = sep), endv, 
        call. = FALSE)
    } else if (type == "message") {
      message(warn, "\n", paste(x[1:nlen], collapse = sep), endv)
    } else if (type == "error") {
      stop(warn, "\n", paste(x[1:nlen], collapse = sep), endv, call. = FALSE)
    }
  }
}

#' Find closest match in a vector
#'
#' @param x numeric vector 
#' @param y vector of numeric values, each of which be matched with the closest
#'     value in \code{x}
#'
#' @return numeric vector of values of \code{x} that are closest to each 
#'     element of \code{y}
#' 
#' @keywords internal
#' @noRd
#' 
closestMatch <- function(x, y) {
    unlist(lapply(y, function(i) {
    x[which(abs(x-i) == min(abs(x-i)))]
  }))
}

#' Check relationship between two variables and identify outliers by sigma 
#'
#' @param x dataframe 
#' @param x_var column name string of explanatory variable
#' @param y_var column name string of response variable
#' @param sigma number of standard deviations in residuals to act as threshold
#'     to define outliers
#' @param plot_output logical; generate plot of relationship with outliers
#' @param output logical; if TRUE a vector of row indices is returned, if
#'     FALSE only a warning
#'
#' @return Vector of row indices showing flagged relationship values,
#'     optionally a plot of the relationship
#' 
#' @export
#' 
relCheck <- function(x, x_var, y_var, sigma = 4, plot_output = FALSE, 
  output = FALSE, warn = "Ratio outside sigma") {
  
  # If no complete cases, break
  if (all(!complete.cases(x[[x_var]], x[[y_var]]))) {
    message("Unable to check relationship, no complete cases")
    return()
  }

  # Linear model
  mod <- lm(x[[y_var]] ~ x[[x_var]], na.action = na.exclude)

  # Extract residuals
  mod_resid <- resid(mod)

  # Extract standard deviation of residuals (sigma)
  mod_sigma <- sigma(mod)

  # Row index with residuals larger than acceptable sigma
  index <- unname(which(
    abs(mod_resid - mean(mod_resid, na.rm = TRUE)) >= (mod_sigma * sigma)
  ))

  # Optionally create plot
  if (plot_output) {
    plot_out <- function() {
      plot(x[,x_var], x[,y_var], 
        type = "p", xlab = x_var, ylab = y_var)
      abline(mod, col = "blue")
      if (length(index) > 0) {
        points(x[index, x_var], x[index, y_var], col = "red", cex = 1)
        text(x[index, x_var], x[index, y_var], labels = index, pos = 3, 
          offset = 0)
      }
    }
  }

  # Send warning message
  warnFormat(index, paste0(warn, ", rows:"), "warning", 15)

  # Return optional outputs
  if (plot_output) {
    return(plot_out())
  } else if (length(index) > 0) {
    if (output) {
      return(index)
    }
  }
}
