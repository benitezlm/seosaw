# Plot table checking functions

#' If plots are nested, are nested values provided consistently
#'
#' @param x dataframe containing plot level measurements
#' @param nested_1 column name string of first level nesting logical
#' @param nested_2 column name string of second level nesting logical
#' @param nested_3 column name string of third level nesting logical
#' @param n1_min_diam_thresh column name string of first level nesting minimum tree diameter
#' @param n1_min_height_thresh column name string of first level nesting minimum tree height
#' @param n1_plot_shape column name string of first level nesting plot shape
#' @param n1_plot_width column name string of first level nesting plot width
#' @param n1_plot_length column name string of first level nesting plot length
#' @param n1_plot_radius column name string of first level nesting plot radius 
#' @param n2_min_diam_thresh column name string of second level nesting minimum tree diameter
#' @param n2_min_height_thresh column name string of second level nesting minimum tree height
#' @param n2_plot_shape column name string of second level nesting plot shape
#' @param n2_plot_width column name string of second level nesting plot width
#' @param n2_plot_length column name string of second level nesting plot length
#' @param n2_plot_radius column name string of second level nesting plot radius
#' @param n3_min_diam_thresh column name string of third level nesting minimum tree diameter
#' @param n3_min_height_thresh column name string of third level nesting minimum tree height
#' @param n3_plot_shape column name string of third level nesting plot shape
#' @param n3_plot_width column name string of third level nesting plot width
#' @param n3_plot_length column name string of third level nesting plot length
#' @param n3_plot_radius column name string of third level nesting plot radius
#' @param output logical; if TRUE a vector of row indices is returned, if FALSE 
#'     only a warning
#'
#' @details Checks whether variables which describe the setup of nested plots
#'     are reported if a plot is recorded as NOT nested at that level (e.g.
#'     \code{nested_1 = FALSE}). 
#' 
#' @return list of vectors of row indices where values aren't logically consistent
#' 
#' @export
#'
nestedCheck <- function(x, nested_1 = "nested_1", nested_2 = "nested_2",
  nested_3 = "nested_3", n1_min_diam_thresh = "n1_min_diam_thresh",
  n1_min_height_thresh = "n1_min_height_thresh", n1_plot_shape = "n1_plot_shape",
  n1_plot_width = "n1_plot_width", n1_plot_length = "n1_plot_length",
  n1_plot_radius = "n1_plot_radius", 
  n2_min_diam_thresh = "n2_min_diam_thresh",
  n2_min_height_thresh = "n2_min_height_thresh", 
  n2_plot_shape = "n2_plot_shape", n2_plot_width = "n2_plot_width", 
  n2_plot_length = "n2_plot_length", n2_plot_radius = "n2_plot_radius", n3_min_diam_thresh = "n3_min_diam_thresh",
  n3_min_height_thresh = "n3_min_height_thresh", n3_plot_shape = "n3_plot_shape",
  n3_plot_width = "n3_plot_width", n3_plot_length = "n3_plot_length",
  n3_plot_radius = "n3_plot_radius", output = TRUE) {


  # Define function, check each level of nesting
  nested_func <- function(x, nested, n) {
    false_pos <- which((
      (x[[get(nested)]] == FALSE & !is.na(x[[get(paste0("n", n, "_min_diam_thresh"))]])) |
      (x[[get(nested)]] == FALSE & !is.na(x[[get(paste0("n", n, "_min_height_thresh"))]])) |
      (x[[get(nested)]] == FALSE & !is.na(x[[get(paste0("n", n, "_plot_shape"))]])) |
      (x[[get(nested)]] == FALSE & !is.na(x[[get(paste0("n", n, "_plot_width"))]])) |
      (x[[get(nested)]] == FALSE & !is.na(x[[get(paste0("n", n, "_plot_length"))]])) |
      (x[[get(nested)]] == FALSE & !is.na(x[[get(paste0("n", n, "_plot_radius"))]]))
    ))

    false_neg <- which((
       (x[[get(nested)]] == TRUE & is.na(x[[get(paste0("n", n, "_plot_shape"))]])) | 
      (x[[get(nested)]] == TRUE & x[[get(paste0("n", n, "_plot_shape"))]] == "rectangle" & 
        (is.na(x[[get(paste0("n", n, "_plot_width"))]]) | is.na(x[[get(paste0("n", n, "_plot_length"))]]))) | 
      (x[[get(nested)]] == TRUE & x[[get(paste0("n", n, "_plot_shape"))]] == "circle" & 
        is.na(x[[get(paste0("n", n, "_plot_radius"))]])) | 
      (x[[get(nested)]] == TRUE & is.na(x[[get(paste0("n", n, "_min_diam_thresh"))]]) & 
        is.na(x[[get(paste0("n", n, "_min_height_thresh"))]]))
    ))

    list(false_pos, false_neg)
  }

  # Create empty list
  n_list <- list()

  # Fill list with row positions
  n_list[c(1,2)] <- nested_func(x, nested_1, 1)
  n_list[c(3,4)] <- nested_func(x, nested_2, 2)
  n_list[c(5,6)] <- nested_func(x, nested_3, 3)

  # Check that nesting hierarchy respected
  n_list[[7]] <- which((
    (x[[nested_1]] == FALSE & x[[nested_2]] == TRUE) |
    (x[[nested_2]] == FALSE & x[[nested_3]] == TRUE) | 
    (x[[nested_1]] == FALSE & x[[nested_3]] == TRUE)
  ))

  # Warning messages
  names(n_list) <- c(
    "nested_1 = FALSE but description values exist",
    "nested_1 = TRUE but description values missing",
    "nested_2 = FALSE but description values exist",
    "nested_2 = TRUE but description values missing",
    "nested_3 = FALSE but description values exist",
    "nested_3 = TRUE but description values missing",
    "Higher level nested = FALSE but lower level(s) = TRUE")

  bad_list <- lapply(1:length(n_list), function(y) {
    warnFormat(n_list[[y]], names(n_list[y]), "warning", 15)
    n_list[[y]]
  })

  if (output) {
    return(bad_list[unlist(lapply(bad_list, function(y) length(y) > 0))])
  }
}

#' If plot is in a PA, is the PA type and PA name specified?
#'
#' @param x dataframe of plot level data
#' @param pa column name string of protected area logical
#' @param pa_type column name string of protected area type
#' @param pa_name column name string of protected area name
#' @param output logical; if TRUE a vector of row indices is returned, if
#'     FALSE only a warning
#'
#' @return list of vectors of row indices where values aren't logically consistent
#' 
#' @export
#'
paCheck <- function(x, pa = "pa", pa_type = "pa_type", pa_name = "pa_name",
  output = TRUE) {
  pa_check <- which(
    ((x[[pa]] == TRUE & is.na(x[[pa_type]])) | 
      (x[[pa]] == TRUE & is.na(x[[pa_name]]))
    )
  )

  warnFormat(pa_check, "PA name or type not specified, rows:", "warning", 15)

  if (output) {
    return(list(pa_check))
  }
}

#' If the plot shape is rectangle, width and length, if circular, radius?
#'
#' @param x dataframe of plot level data
#' @param plot_shape column name string of plot shape
#' @param plot_width column name string of plot width
#' @param plot_length column name string of plot length
#' @param plot_radius column name string of plot radius
#' @param output logical; if TRUE a vector of row indices is returned, if
#'     FALSE only a warning
#'
#' @return list of vectors of row indices where values aren't logically consistent
#' 
#' @export
#'
shapeCheck <- function(x, plot_shape = "plot_shape", 
  plot_width = "plot_width", plot_length = "plot_length", 
  plot_radius = "plot_radius", output = TRUE) {
 
  shape_list <- list()

  # Rectangle but containing circle radius 
  shape_list[[1]] <- which(
    ((x[[plot_shape]] == "rectangle" & !is.na(x[[plot_radius]])))
  )

  # Circle but containing length and width
  shape_list[[2]] <- which(
    ((x[[plot_shape]] == "circle" & !is.na(x[[plot_length]])) | 
    (x[[plot_shape]] == "circle" & !is.na(x[[plot_width]]))) 
  )

  # Circle but not containing radius 
  shape_list[[3]] <- which(
    ((x[[plot_shape]] == "circle" & is.na(x[[plot_radius]])))
  )

  # Rectangle but not containing length and width
  shape_list[[4]] <- which(
    ((x[[plot_shape]] == "rectangle" & is.na(x[[plot_length]])) | 
    (x[[plot_shape]] == "rectangle" & is.na(x[[plot_width]]))) 
  )

  names(shape_list) <- c(
    "plot_shape == 'rectangle' but plot radius is filled",
    "plot_shape == 'circle' but plot length and/or width is filled",
    "plot_shape == 'circle' but plot radius not filled",
    "plot_shape == 'rectangle' but plot length and/or width not filled")

  bad_list <- lapply(1:length(shape_list), function(y) {
    warnFormat(shape_list[[y]], names(shape_list[y]), "warning", 15)
    shape_list[[y]]
  })

  if (output) {
    return(bad_list[unlist(lapply(bad_list, function(y) length(y) > 0))])
  }
}

#' If fire exclusion or manipulation, is the success and treatment described?
#'
#' @param x dataframe containing plot level data
#' @param fire_return_interval column name string of natural fire return interval
#' @param fire_exclusion column name string of fire exclusion logical 
#' @param fire_exclusion_success column name string of fire exclusion success estimate
#' @param fire_treatment column name string of fire treatment logical
#' @param fire_treatment_return_interval column name string of fire treatment 
#'     return interval
#' @param fire_treatment_season column name string of fire treatment season
#' @param output logical; if TRUE a vector of row indices is returned, if
#'     FALSE only a warning
#'
#' @return list of vectors of row indices where values aren't logically consistent
#' 
#' @export
#' 
fireCheck <- function(x, fire_exclusion = "fire_exclusion", 
  fire_exclusion_success = "fire_exclusion_success", 
  fire_treatment = "fire_treatment", 
  fire_return_interval = "fire_return_interval", 
  fire_treatment_season = "fire_treatment_season", 
  output = TRUE) {

  fire_list <- list()

  fire_list[[1]] <- which(
    ((x[[fire_exclusion]] == TRUE & is.na(x[[fire_exclusion_success]])))
  )

  fire_list[[2]] <- which(
    ((x[[fire_exclusion]] == FALSE & !is.na(x[[fire_exclusion_success]])))
  )

  fire_list[[3]] <- which(
    ((x[[fire_treatment]] == TRUE & is.na(x[[fire_treatment_season]])) | 
      (x[[fire_treatment]] == TRUE & is.na(x[[fire_return_interval]])))
  )

  fire_list[[4]] <- which(
    ((x[[fire_treatment]] == FALSE & !is.na(x[[fire_treatment_season]])))
  )


  names(fire_list) <- c(
    "fire_exclusion == TRUE but fire_exclusion_success is NA",
    "fire_exclusion == FALSE but fire_exclusion_success is filled",
    "fire_treatment == TRUE but season and/or return interval are NA",
    "fire_treatment == FALSE but season is filled")

  bad_list <- lapply(1:length(fire_list), function(y) {
    warnFormat(fire_list[[y]], names(fire_list[y]), "warning", 15)
    fire_list[[y]]
  })

  if (output) {
    return(bad_list[unlist(lapply(bad_list, function(y) length(y) > 0))])
  }
}

#' Does country ISO3C code match coordinates?
#'
#' @param x dataframe of plot level data
#' @param country_iso3 column name string of country ISO3C code
#' @param longitude_of_centre column name string of plot longitude
#' @param latitude_of_centre column name string of plot latitude
#' @param output logical; if TRUE a vector of row indices is returned, if
#'     FALSE only a warning
#' @param ... extra arguments passed to \code{adminQuery()}
#'
#' @return list of vectors of row indices showing flagged rows 
#' 
#' @importFrom sf st_as_sf st_intersects st_crs st_drop_geometry
#' @export
#' 
isoCheck <- function(x, country_iso3 = "country_iso3",
  longitude_of_centre = "longitude_of_centre", 
  latitude_of_centre = "latitude_of_centre", EPSG = 4326, output = TRUE, ...) {

  # Get Africa polygons
  africa <- get(data(africa))

  # Convert plots to points sf
  x_sf <- sf::st_as_sf(x, coords = c(longitude_of_centre, latitude_of_centre), na.fail = FALSE)
  sf::st_crs(x_sf) <- EPSG 

  # Get intersections and clean
  matches <- sf::st_intersects(x_sf, africa)
  matches[sapply(matches, function(y) {length(y) < 1 })] <- NA
  matches <- unlist(matches)
  matches <- sf::st_drop_geometry(africa[matches,"iso3"])[[1]]

  # Does matches correspond with country_iso3?
  unmatch <- which(x[[country_iso3]] != matches)
  warnFormat(unmatch, 
    "Plot coordinates do not match country ISO3C code, rows:",
    "warning", 15)

  # Optionally return indices
  if (output) {
    return(list(unmatch))
  }
}

#' Are \code{first_census} and \code{last_census} dates in \code{all_dates}
#'
#' @param x dataframe of plot level data
#' @param all_dates column name string of all census dates
#' @param first_census column name string of first census 
#' @param last_census column name string of last census 
#' @param output logical; if TRUE a vector of row indices is returned, if
#'     FALSE only a warning
#'
#' @return list of vectors of row indices showing flagged rows 
#' 
#' @export
#' 
dateCheck <- function(x, all_dates = "all_dates", first_census = "first_census",
  last_census = "last_census", output = TRUE) {

  # Split all_dates
  all_dates_split <- strsplit(x[[all_dates]], ";")

  # For each plot, which dates match 
  matches <- lapply(seq_along(all_dates_split), function(y) {
    dates <- sort(all_dates_split[[y]])

    list(x[y, first_census] == dates[1],
      x[y, last_census] == dates[length(dates)])
  })

  date_list <- list()

  date_list[[1]] <- which(!unlist(lapply(matches, "[", 1)))
  date_list[[2]] <- which(!unlist(lapply(matches, "[", 2)))

  names(date_list) <- c(
    "first_census != earliest date in all_dates, rows:",
    "last_census != latest date in all_dates, rows:"
    )

  bad_list <- lapply(seq_along(date_list), function(y) {
    warnFormat(date_list[[y]], names(date_list[y]), "warning", 15)
    date_list[[y]]
  })

  # Optionally return row indices
  if (output) {
    return(bad_list[unlist(lapply(bad_list, function(y) length(y) > 0))])
  }
}

#' Run a number of table-level data quality checks on plot-level data
#'
#' @param x dataframe containing plot level data  
#' @param funs named list of functions to check plot-level data
#' @param ... additional arguments passed to functions listed in \code{funs}
#' 
#' @return list of vectors of row indices with logical inconsistencies 
#'     by each function in \code{funs} 
#'
#' @export
#'
plotTableCheck <- function(x, 
  funs = list("nestedCheck" = nestedCheck, "paCheck" = paCheck, 
    "shapeCheck" = shapeCheck, "fireCheck" = fireCheck, 
    "isoCheck" = isoCheck), ...) {
  lapply(1:length(funs), function(y) {
    message("Running:", names(funs[y]))
    funs[[y]](x, ...)
  })
}

