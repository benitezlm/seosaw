# Column validity functions for basic stem table columns

#' Check validity of stem genus column
#'
#' @param x vector of stem genera
#' @param ... optional arguments passed to \code{coerceCatch()} and \code{naCatch()}
#'
#' @return Vector of class "character"
#' @keywords internal
#' @noRd
#'
genus <- function(x, ...) {
  x <- fillNA(x)
  x <- coerceCatch(x, as.character, ...)
  naCatch(x, warn = TRUE, ...)
  if (any(!grepl("^[[:alpha:]]+$", x[!is.na(x)]))) {
    warning("Non-letter characters found in genus")
  }
  else if (any(!grepl("^[A-Z]", x[!is.na(x)]))) {
    warning("Genera must start with a capital letter [A-Z]")
  }
  else if (any(grepl("[A-Z]", substring(x[!is.na(x)], 2)))) {
    warning("Genera must not have multiple capital letters")
  }
  structure(x, class = "character")
}

#' Check validity of stem species column
#'
#' @param x vector of stem species'
#' @param ... optional arguments passed to \code{coerceCatch()} and \code{naCatch()}
#'
#' @return Vector of class "character"
#' @keywords internal
#' @noRd
#'
species <- function(x, ...) {
  x <- fillNA(x)
  x <- coerceCatch(x, as.character, ...)
  x <- tolower(x)
  naCatch(x, warn = TRUE, ...)
  if (any(!grepl("^[a-z]+(-[a-z]+)?$", x[!is.na(x)]))) {
    warning("Non-letter characters found in species")
  }
  else if (any(grepl("[A-Z]", x[!is.na(x)]))) {
    warning("Species must not contain capital letters [A-Z]")
  }
  structure(x, class = "character")
}

#' Check validity of subspecies column
#'
#' @param x vector of stem subspecies
#' @param ... optional arguments passed to \code{coerceCatch()} and \code{naCatch()}
#'
#' @return Vector of class "character"
#' @keywords internal
#' @noRd
#' 
subspecies <- function(x, ...) {
  x <- fillNA(x)
  x <- coerceCatch(x, as.character, ...)
  naCatch(x, warn = TRUE, ...)

  if (any(grepl("[A-Z]", x[!is.na(x)]))) {
    warning("Subspecies must not contain capital letters [A-Z]")
  }
  else if (any(grepl("subsp\\.", x[!is.na(x)]), grepl("var\\.", x[!is.na(x)]))) {
    warning("Subspecies must not contain prefixes such as 'subsp.' or 'var.'")
  }
  structure(x, class = "character")
}

#' Check validity of variety column
#'
#' @param x vector of stem variety
#' @param ... optional arguments passed to \code{coerceCatch()} and \code{naCatch()}
#'
#' @return Vector of class "character"
#' @keywords internal
#' @noRd
#' 
variety <- function(x, ...) {
  x <- fillNA(x)
  x <- coerceCatch(x, as.character, ...)
  naCatch(x, warn = TRUE, ...)

  if (any(grepl("[A-Z]", x[!is.na(x)]))) {
    warning("Variety must not contain capital letters [A-Z]")
  }
  else if (any(grepl("subsp\\.", x[!is.na(x)]), grepl("var\\.", x[!is.na(x)]))) {
    warning("Variety must not contain prefixes such as 'subsp.' or 'var.'")
  }
  structure(x, class = "character")
}

#' Check validity of date of measurement column
#'
#' @param x vector of measurement dates
#' @param ... optional arguments passed to \code{coerceCatch()} and \code{naCatch()}
#'
#' @return Vector of class "Date"
#' @keywords internal
#' @noRd
#'
measurement_date <- function(x, ...) {
  x <- fillNA(x)
  x <- coerceCatch(x, as.integer, ...)
  naCatch(x, ...)
  if (any(!grepl("^[0-9]{4}$", x))) {
    stop(paste("Measurement date not a year:", 
        paste(x[!grepl("^[0-9]{4}$", x)], collapse = ", ")))
  }
  structure(x, class = "numeric")
}

#' Check validity of stem diameter point of measurement (POM) column
#'
#' @param x vector of diameter measurement POMs
#' @param ... optional arguments passed to \code{double_val()}, \code{naCatch()}
#'     and \code{negCatch()}
#'
#' @return Vector of class "numeric"
#' @keywords internal
#' @noRd
#'
pom <- function(x, ...) {
  x <- double_val(x, ...)
  negCatch(x, zero = TRUE, ...)
  naCatch(x, warn = TRUE, ...)
  if (any(x[!is.na(x)] > 5)) {
    warning("POM over 5 m, is this correct?")
  }
  structure(x, class = "numeric")
}

#' Check validity of stem diameter column
#'
#' @param x vector of diameter measurements 
#' @param ... optional arguments passed to \code{double_val()} 
#'
#' @return Vector of class "numeric"
#' @keywords internal
#' @noRd
#'
diam <- function(x, ...) {
  x <- double_val(x, ...)
  negCatch(x, zero = TRUE, ...)
  if (any(x[!is.na(x)] > 100)) {
    warning("Diameter over 100 cm, is this correct?")
  }
  structure(x, class = "numeric")
}

#' Check validity of stem height column
#'
#' @param x vector of height measurements 
#' @param ... optional arguments passed to \code{double_val()} 
#'
#' @return Vector of class "numeric"
#' @keywords internal
#' @noRd
#'
height <- function(x, ...) {
  x <- double_val(x, ...)
  negCatch(x, ...)
  if (any(x[!is.na(x)] > 50)) {
    warning("Height over 50 m, is this correct?")
  }
  structure(x, class = "numeric")
}

#' Check validity of crown dimension column
#'
#' @param x vector of crown dimension measurements 
#' @param ... optional arguments passed to \code{double_val()} 
#'
#' @return Vector of class "numeric"
#' @keywords internal
#' @noRd
#'
crown_dim <- function(x, ...) {
  x <- double_val(x, ...)
  negCatch(x, ...)
  if (any(x[!is.na(x)] > 25)) {
    warning("Crown dimension over 25 m, is this correct?")
  }
  structure(x, class = "numeric")
}

#' Check validity of stem alive or dead column
#'
#' @param x vector of stem alive or dead values
#' @param ... optional arguments passed to \code{genericFactor()} 
#'
#' @return Vector of class "factor"
#' @keywords internal
#' @noRd
#'
alive <- function(x, ...) {
  x <- tolower(x)
  genericFactor(x, "alive", ...)
}

#' Check validity of stem stem status column
#'
#' @param x vector of stem stem status
#'
#' @return Vector of class "factor"
#' @keywords internal
#' @noRd
#'
stem_status <- function(x, ...) {
  x <- tolower(x)
  genericFactor(x, "stem_status", ...)
}

#' Check validity of stem standing or fallen column
#'
#' @param x vector of standing or fallen values
#' @param ... optional arguments passed to \code{genericFactor()} 
#'
#' @return Vector of class "factor"
#' @keywords internal
#' @noRd
#'
standing_fallen <- function(x, ...) {
  x <- gsub("Standing", "S", x, ignore.case = TRUE)
  x <- gsub("Fallen", "F", x, ignore.case = TRUE)
  x <- tolower(x)
  genericFactor(x, "standing_fallen", ...)
}

#' Check validity of stem stem mode column
#'
#' @param x vector of stem stem mode values
#' @param ... optional arguments passed to \code{genericFactor()} 
#'
#' @return Vector of class "factor"
#' @keywords internal
#' @noRd
#'
stem_mode <- function(x, ...) {
  x <- tolower(x)
  genericFactor(x, "stem_mode", ...)
}

#' Check validity of stem damage cause column
#'
#' @param x vector of stem damage cause values
#' @param ... optional arguments passed to \code{genericFactor()} 
#'
#' @return Vector of class "factor"
#' @keywords internal
#' @noRd
#'
damage_cause <- function(x, ...) {
  x <- tolower(x)
  genericFactor(x, "damage_cause", ...)
}

#' Check validity of stem human damage cause column
#'
#' @param x vector of stem human damage cause
#' @param ... optional arguments passed to \code{genericFactor()} 
#'
#' @return Vector of class "factor"
#' @keywords internal
#' @noRd
#'
damage_cause_human <- function(x, ...) {
  genericFactor(x, "damage_cause_human", ...)
}

#' Check validity of stem decay column
#'
#' @param x vector of stem decay values
#' @param ... optional arguments passed to \code{double_val()} 
#'
#' @return Vector of class "numeric"
#' @keywords internal
#' @noRd
#'
decay <- function(x, ...) {
  x <- double_val(x, ...)
  if (any(x[!is.na(x)] < 0)) {
    stop("All stem decay values must be non-negative")
  }
  else if (any(x[!is.na(x)] > 5)) {
    stop("Stem decay values must be less than 5")
  }
  else if (any(!x[!is.na(x)] %in% seq(0, 5, by = 0.5))) {
    stop("Stem decay values must be rounded to 0.5")
  }
  structure(x, class = "numeric")
}

#' Check validity of stem diameter method column
#'
#' @param x vector of stem diameter method values
#' @param ... optional arguments passed to \code{genericFactor()} 
#'
#' @return Vector of class "factor"
#' @keywords internal
#' @noRd
#'
diam_method <- function(x, ...) {
  genericFactor(x, "diam_method", ...)
}

#' Check validity of stem height method column
#'
#' @param x vector of stem height method values
#' @param ... optional arguments passed to \code{genericFactor()} 
#'
#' @return Vector of class "factor"
#' @keywords internal
#' @noRd
#'
height_method <- function(x, ...) {
  genericFactor(x, "height_method", ...)
}

#' Check validity of stem coordinate method column
#'
#' @param x vector of stem coordinate method values
#' @param ... optional arguments passed to \code{genericFactor()} 
#'
#' @return Vector of class "factor"
#' @keywords internal
#' @noRd
#'
xy_method <- function(x, ...) {
  genericFactor(x, "xy_method", ...)
}

#' Check validity of Fraction Probability of Inclusion
#'
#' @param x vector of FPC values
#' @param ... optional arguments passed to \code{double_val()} 
#'
#' @return Vector of class "numeric"
#' @keywords internal
#' @noRd
#' 
fpc <- function(x, ...) {
  x <- pos_double_val(x, ...)
  x[is.na(x)] <- 1
  x <- prop_val(x)
  structure(x, class = "numeric")
}

#' Check validity of X,Y grid coordinates 
#'
#' @param x vector of x or y grid coord. 
#' @param ... optional arguments passed to \code{coerceCatch()} and \code{naCatch()}
#'
#' @return Vector of class "numeric"
#' @keywords internal
#' @noRd
#' 
xy_grid <- function(x, ...) {
  x <- fillNA(x)
  x <- coerceCatch(x, as.double, ...)
  naCatch(x, warn = TRUE, ...)
  structure(x, class = "numeric")
}

#' Check validity of angular coordinates 
#'
#' @param x vector of degree angles ranging 0-360
#' @param ... optional arguments passed to \code{coerceCatch()} and \code{naCatch()}
#'
#' @return Vector of class "numeric"
#' @keywords internal
#' @noRd
#' 
angle <- function(x, ...) {
  x <- fillNA(x)
  x <- coerceCatch(x, as.double, ...)
  naCatch(x, warn = TRUE, ...)
  if (any(x[!is.na(x)] > 360)) {
    stop(paste("Angles must not be greater than 360 degrees:", 
        paste(x[x > 360], collapse = ", ")  ))
  }
  structure(x, class = "numeric")
}
