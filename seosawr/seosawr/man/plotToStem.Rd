% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/plot_from_stems.R
\name{plotToStem}
\alias{plotToStem}
\title{Add plot level columns to a stems table}
\usage{
plotToStem(
  x,
  plot_data,
  cols,
  stem_plot_id = "plot_id",
  plot_plot_id = stem_plot_id
)
}
\arguments{
\item{x}{dataframe of stem measurements}

\item{plot_data}{dataframe of plot level data}

\item{cols}{character vector of column names from plot data to add to 
stem data}

\item{stem_plot_id}{column name string of plot IDs in stem data}

\item{plot_plot_id}{column name string of plot IDs in plot data}
}
\value{
Dataframe with extra columns
}
\description{
Duplicates values of a plot level metadata column and appends to a stem 
    level dataframe, based on shared values of plot ID
}
