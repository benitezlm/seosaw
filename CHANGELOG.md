# CHANGELOG

Notable changes to the SEOSAW dataset, seosawr R package, [SEOSAW website](https://seosaw.github.io).

## TODO

* Add greater detail on determining alive vs. dead trees and stems to the Field Manual. It would be the logical thing to infer that if a stem is "T" that the base of the tree is "A", as confirmed by some other stem being "A", or the tree being "R",  but in practice in previous censuses I don't think that's been applied consistently, and that logic isn't in the field manual yet. So I think we should simplify to stem alive. stem dead but base resprouting. stem and base dead. Because if the base isn't resprouting then its a guess as to if it is alive. See SEOSAW v3 doc in `doc/plan/`


## [2.12] - 2021-03-17

### Added

* ODK template form for recensusing a plot - `stems_recensus.xls`
* McNicol Kilwa dataset. 56 one-off circular 25 m radius plots. - `TKN`
* Regional report RMarkdown template. Feed it a .shp polygon to generate a report of SEOSAW's involvement in the area.
* Dataset report Rmarkdown template. Feed it a dataset ID (e.g. "SSM") to generate useful data visualisations for that dataset.
* Added `canopy_climbers`, `stem_stranglers`, `on_termites`, `liana` questions to all stems ODK forms
* Added `alive` question to all stems ODK forms. This question becomes relevant if topkilled and not-resprouting
* `polyPlotCheck()` to see if polygon areas match roughly with `plot_area` generated from `plot_length`, `plot_width`, `plot_radius`. Also that distance between centres of plot in plots data is similar to centre of polygon.
* `baobab` column in plots data, to check if a plot contains _Adansonia digitata_, which can mess up biomass estimations.
* New "data quality" table output in `data_out/plot_qual.rds`, with calculated values to show data coverage and number of measurements with possible errors in the dataset. Table generated by `cleaning/qual.R`.
* `saplings_sampled` - TRUE/FALSE whether stems smaller than the minimum diameter threshold were counted.
* `wood_density` - wood density per stem taken from Zanne et al. 2009 global wood density database. Calculated with `stemExtraColGen()`, which sources `woodDensity()`

### Changed

* `plots.csv` is now organised as one row per census per plot, with a new column `census_id` which acts as the primary key. `plots_latest.csv` is a subsidiary of `plots.csv` which only contains plot-level statistics for the most recent census. This change is seen as a baby step towards the eventual goal of having a separate "census" table (census level calculated statistics) and "plot" table (plot level metadata). Additionally, this way the default object `plots.csv` contains data on all censuses, so users can flexibly subset by census.
* Circle polys generated with `polyGen()` were previously too large due to an error in the way `sf::st_buffer()` was applied.
* Nested subplot measurements removed from `CBN_*` as they were surveyed inconsistently across censuses and caused aritificial inflation of stocking density
* Removed four plots in `MCF` with unknown plot dimensions.
* `plot_diameter` -> `plot_radius` - plays better with arbitrary `{sf}` functions and more natural to measure in the field in a circular plot.
* Removed all `SHS` plots from compiled dataset.
* Removed `ZPF_10`, `ZPF_11`, `ZPF_12`, `ZPF_13`, `ZPF_14`, `ZPF_15`, `ZGF_30`, `ZGF_33`, `ZGF_35`, `ZGF_37`, `ZGF_45` due to lack of plot location
* Checked through all multi-census datasets and removed backdated recruits, with NA diameter measurements
* Minor updates to stems ODK form variants to flag diameter values >100 cm
* Stems recensus ODK form now handles tags not in original data more gracefully.

### Issues

* `MCL`: possible that some stems should be dead in future censuses, as they are without diameter measurements. It's also possible that the stem IDs are completely wrong in the 2000 census. This issue is also present in previous versions
* `SBM`: stem diameters are actually circumferences, uncorrected as of 2021-03-16

## [2.11] - 2021-02-17

### Added

* Code to PI names from SEOSAW plots dataset for use in website
* `dateCheck()` to see if `first_census`, `last_census` and `all_dates` are logically consistent
* New columns 
	* >=20 cm diameter variants for diameter, height, stocking density, note names changed from `gt` to `ge` to reflect "greater than or equal to" rather than the old "greater than":
		* `diam_ge20_max`
		* `diam_ge20_min`
		* `diam_ge20_mean`
		* `diam_ge20_median`
		* `diam_ge20_sd`
		* `height_ge20_max`
		* `height_ge20_min`
		* `height_ge20_mean`
		* `height_ge20_median`
		* `height_ge20_sd`
		* `stocking_ge20_ha`
	* >=3,5,10,20 cm diameter variants of `agb_ha`:
		* `agb_ge3_ha`
		* `agb_ge5_ha`
		* `agb_ge10_ha`
		* `agb_ge20_ha`
	* `height_per95` - the 95th percentile of stem height
	* `diam_scale`, `diam_scale_se`, `weibull_scale`, `weibull_shape`, `weibull_scale_se`, `weibull_shape_se` - columns which describe the shape of stem diameter class distributions for each plot. See `diamWeibullGen()` and `diamScaleGen()` functions which define these columns.
	* `coarse_woody_debris_sampled` - TRUE/FALSE whether plot had coarse woody debris sampled.
* `plots_subdiv.csv` and `polys_subdiv.shp` provide 1 ha subdivisions of plots >2 ha, rectangular and divisible by 100x100m squares. `largePlotSplit()` does the splitting. `plots_subdiv.csv` provides latest census calculated fields (e.g. `agb_ha`) for these new subplots. A census level version of `plots_subdiv.csv` may be added later.
* `gridCoordGen()` - approximates an XY grid coordinate system in ractangular plots which have GPS stem positions
* `plot_clusters.csv` - Assigns plots to clusters based on genus stem abundances, using Ward clustering. `clusters_dom.csv` gives the dominant genera for each cluster., based on mean stem abundance across all plots in cluster.
* `build.sh` - shell script for compiling a version of the SEOSAW dataset
* Dataset-level long-form descriptions of each dataset in the Dataset Manual
* `doc/versions.csv` spreadsheet which takes snapshots of the versions of all the versioned objects in SEOSAW to keep them together

### Changed

* Columns which contain a list of items now separate items by ";":
	* `prinv`
	* `other_authors`
	* `all_dates`
* `other_authors` and `prinv` values cleaned to maintain consistency. E.g. "Godlee, JL" -> "Godlee J. L.", "John Godlee" -> "Godlee J. L." 
* Backdated recruits with `diam` == NA in `MGR_*` removed from dataset
* Forward dated unmeasured stems removed from various plots: `CBN_3`, `CBN_6`, `MNR_14`, `MNR_41`, etc.
* Corrected `all_dates` for a number of plots which were causing issues with the new `subsetLatest()` code: `MAR`, `MCL`, `ZRF`, `CBN`, `TIM`
* `first_census` and `last_census` auto-generated from `all_dates` by `plotExtraColGen()`
* `ba` and `agb` (not per ha) cols removed from plots table as they can lead to confusion.
* Functionality from `emptyCatch()` added into `fillNA()`, handles conversion of Inf, NaN, NA, "N/A", "na", "   ", and optionally "0", to NA. Maintains class of original vector.
* `CLP_*` plots changed to `DLP_*` to reflect location in the Democratic Republic of Congo (D), rather than the Republic of Congo (C).
* Fixed `fpc` values for `SHD_*` plots. Previously AGB estimates were falsely inflated. New AGB values are much lower than previous.
* Overhauled functions to generate fake data, now in one function called `exampleDataGen()`.
* Overhauled `plotStemColGen()` (was `plotStemColumnGen()`) so that it handles empty entries more consistently, correctly differentiates which columns should be 0 if empty (e.g. `n_stems`) and which should be NA if empty (e.g. `lorey_height` (e.g. `lorey_height`)).
* Changes to field paper data sheets `blank_plot.pdf` and `blank_stem.pdf`. These forms are now given version numbers and archived in the same way as manuals on the website with a `_latest` version.
	* Removed max thresholds for diameter and height. 
	* Added explicit prompts for nested plot dimensions and sampling thresholds
	* Added site name 
	* Location data now for each corner
	* Added "Coarse woody debris" to sampling TRUE/FALSE section
	* Added "PI" 
	* Added "A - alive" to stem status table
	* Added "Trees tagged" 
	* Added form version number and field manual version to form footer on every page

## [2.10] - 2021-01-21

### Added

* Extra check during `dataExport()` to make sure stems and plot tables have the full complement of columns
* ODK forms, one for plot data and one for stem data. Found in `doc/manuals/forms/odk/`. Alternative variants of stems form which is compatible with Kobotoolbox, doesn't use the auto-populating species list feature, instead takes a pre-defined list of species, and another which uses the CJB african plant species list.
* Added function to check for duplicated measurement IDs (`measurement_id`) during cleaning - `measurementIDCheck()`
* New datasets: 
	* Wilson Mugasha: 12 plots part of a woodland regeneration experiment near Morogoro, Tanzania. `TKM_*`
	* Antje Ahrends: Transects made into plots in Tanzanian coastal forest (?). `TLA_*`
* New columns:
	* TRUE/FALSE columns, to keep in line with ForestPlots.net:
    	* `recruit` 
    	* `on_termites` 
    	* `lightning` 
    	* `canopy_climbers` 
    	* `stem_stranglers` 
    	* `stump` 
    	* `bracket_fungi`
	* `manipulation_experiment_notes` - provide notes of experimental treatment applied to plot.
	* `cambium` - proportion of stem surface with cambium exposed between 0-2 m from ground
* New options in columns, to keep in line with ForestPlots.net:
	* `stem_mode`: "x" - Presumed dead, location not found e.g. due to poor maps
	* `damage_cause`: "l" - Lightning
* New spatial data columns: `distance_road` and `type_nearest_road` - distance to nearest road and road type of nearest road, from Meijer et al. Global patterns of current and future road infrastructure. (2018). Environmental Research Letters.

### Changed

* Repository made open access
* `seosawr` package vignette, data contribution manual, and dataset description joined together into one "dataset manual" as a .pdf file only, using Sweave instead of RMarkdown
* Re-organised manuals and docs into `doc/`
* Field sheets and data entry templates now found in `doc/manuals/forms/`
* Column names:
	* `sampling_design_fp_code` -> `sampling_design_code` - no reason why this column should be limited to ForestPlots protocols
	* `stem_decay` -> `decay` - "stem" is redundant as in stems table
	* `plot_slope` -> `slope` - "plot" is redundant as in plots table
	* `plot_aspect` -> `aspect` - "plot" is redundant as in plots table
* Maiato Cusseque plots FPC set to 1. Previously was an error with FPC increasing by row number due to dplyr update
* Added "other" as a factor option to `damage_cause_human`, `height_method`, `diam_method`, `xy_method`, `slope_method`, `canopy_cover_method`
* `vecExtract()` renamed to `vectorExtract()` to match style of `rasterExtract()`
* In `speciesGen()`, if genus is a family and species is indet, record genus as "Indet". Based on list of known families generated by `seosawFamilyGen()`. 
* Fixed issue with duplicated measurement IDs in 'MAR_*', 'MCL_*', 'ZCC_*', 'DKS_*', 'TKX_*', 'TKW_*'
* All factor codes (`alive`, `stem_status`, `standing_fallen`, `stem_mode`, `damage_cause`) replaced with lower case variants. Upper case "F" values can cause `read.csv()` to interpret them as logical "FALSE", which is undesirable. Column constructor functions now automatically convert to these lower case variants where they match 
* `speciesGen()` now corrects "indet" and "Indet" to "Indet indet"
* `ACM_*` plots have corrected biomass, previously erroneously low due to many `stem_id == NA`
* `analysis` directory removed from repository
* Spatial data column `ma_city` replaced with `travel_time_city` and  `travel_time_settlement`, using the updated data from Nelson et al. (2019) A suite of global accessibility indicators. Scientific Data. City classed as >50000 people, settlement as >5000 people

## [2.9] - 2020-11-07

### Added

* SEOSAW region polygon as data object in `{seosawr}`: `data(seosaw_region)`
* SEOSAW region bounding box as data object in `{seosawr}`: `data(seosaw_bbox)`
* Fire return interval now applicable to natural and treatment fires.
* `CHANGELOG.md` to track changes in SEOSAW database, `{seosawr}` and the website.
* New columns
	* plots: `plot_aspect` - column with degree angle value
* Set of blank SEOSAW style datasheets for the field, with companion data entry sheets, and a program to create re-census field datasheets from existing SEOSAW style data. Tested with 'ABG_*', 'TKW_*', and 'CAR_*' datasets. Blank sheets hosted on website, program still private.
* Polygons now auto-generated for those plots which have no polygons, based on `longitude_of_centre`, `latitude_of_centre`, `plot_shape`, `plot_diameter`, `plot_length`, `plot_width`.
* `circlePolyGen()` -> `polyGen()` - now handles circle and rectangle plot shapes, provided they aren't missing any necessary data
* `versionCheck()` to facilitate checking of plot datasets

### Changed

* Column names:
	* plots: `fire_treatment_return_interval` -> `fire_return_interval` - allow non-treatment estimates of fire return interval
	* plots: `fire_control` -> `fire_treatment` - "control" implied that fires are only suppressed
	* plots: `method_canopy_cover` -> `canopy_cover_method` - for consistent ordering of terms
	* plots: `method_canopy_cover_notes` -> `canopy_cover_method_notes` - for consistent ordering of terms
	* stems: `xy_latlon_method` -> plots: `xy_method` 
    * plots: `mean_plot_slope` -> `plot_slope`
	* stems: `method_diam` -> `diam_method` - for consistent ordering of terms
	* stems: `method_height` -> `height_method` - for consistent ordering of terms
* Relaxed restrictions on `plot_id`, just three unique letters and an integer, doesn't have to be initials.
* Spatial data layers removed from `{seosawr}`, now stored separately and extracted using data in `seosaw_data/data_out` rather than `seosaw_data/data_clean/*` 
* Max and min lat-long coordinates of plot locations are now defined according to `data(seosaw_bbox)`.
* Abundance matrix generator function (`abMatGen()`) replaced. Removed dependency on `{dplyr}` and `{tidyr}`. Faster and more terse code.
* Dataset compilation script `compile.sh` replaced with more flexible `Makefile`, but this feature is still unfinished
* Website contact form migrated from SimpleForm to Formspree
* `vecExtract()` uses faster matching with `st_intersects()`, rather than `st_join()`
* `EPSG2CRS` now returns WKT by default, rather than proj4string.
* All spatial extraction functions can now handle non-EPSG CRS.
* `stemTreeIDClean()`, `tree_id()`, `stem_id()`, `measurement_id()` -> `idGen()`. Removes bloated dependencies on `{dplyr}`, better handling of NA `stem_id` and `tree_id` values
* Re-censuses are now cleaned in the same site-level cleaning script as original data, to make cross-checking easier.
* `NA` for `polygons` in "SSM" "MCF" "MNF" "ZHH" "MCL" "TIM" "DKS" "ZWP" "MNR" "MGR" "ZKS" "SBT" "ZGF" "ZPF" has been corrected, all now either TRUE or FALSE.
* Corrected nesting and `fpc` values for Zimbabwe FC and Twine SUCSES datasets
* `mstems` and `mplots` example datasets re-created to reflect above changes
* Better stem matching between censuses in Kilwa and Nhambita datasets, thanks to Thom Brade
* Estimates of basal area, AGB, number of stems reduced in `DKS_1` due to fixing census dates
* Lower AGB, basal area, in SHD plots due to fixed `fpc` 
* New plots in Mozambique NFI (`MNI`) dataset and Vera De Cauwer's Namibia dataset (`NCC`)
* Archibald Kruger dataset vastly reduced `n_determ_species` because most recent census only looked at large trees. Plot level statistics are generated from most recent census only


## [2.8] - 2020-09-10

### Added

* Vera De Cauwer's Angola/Namibia plot data
* Alternative stem coordinate system for circular plots - `distance` and `angle` from plot centre.
* `permanent` column, logical which defines whether a plot is permanent or not. 
	* Website dataset description reflects addition of `permanent` column
	* Website map reflects new `permanent` column.
* Moved some website-making scripts to the SEOSAW Bitbucket repo.

### Changed

* Column names:
	* `first_census_date` -> `first_census`
	* `last_census_date` -> `last_census`
* Kilwa plots: `TKW_4`, `TKW_19`, `TKW_22`, included in polygons dataset. Previously excluded because plot centre coordinates didn’t fall within the polygon co-ordinates, this requirement has now been relaxed and instead matches based on the nearest polygon. 
* Subtly changes the description of nested plots, particularly plot minimum diameter threshold (`min_diam_thresh`). 
* Data cleaning scripts now clean polygons per site alongside other data, rather than in a polygons-only script. 

