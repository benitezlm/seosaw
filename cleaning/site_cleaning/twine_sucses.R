# Clean Twine SUCSES data
# John Godlee (johngodlee@gmail.com)
# 2020-05-15

# Packages
library(dplyr)
library(seosawr)
library(readxl)

source("../functions.R")  

# Import data
plots <- read_xlsx(file.path(args[1], "twine_sucses/36_southafrica_twine_SUCSES_checkY.xlsx"),
  sheet = 2)
stems <- read.csv(file.path(args[1], "twine_sucses/36_southafrica_bushbuck_twine_clean.csv"))

# Plot cleaning ----

# Rename and create columns
plots_clean <- old_plot_col_repl(plots) %>%
  colGen(., plotColClass())

# Fix values
plots_clean_val <- plots_clean %>%
  mutate(
    plot_radius = plot_radius * 2,
    plot_id = gsub("-(0)?(0)?", "_", .$plot_id),
    plot_clust = gsub("[0-9]", "", .$plot_name)) %>%
  group_by(plot_clust) %>%
  mutate(plot_cluster = na.omit(first(plot_id))) %>%
  ungroup() %>%
  dplyr::select(-plot_clust) %>%
  mutate(
    date_metadata = 2017,
    prinv = gsub(",", "", prinv),
    other_authors = gsub(",", "", other_authors),
    subplot_n = 4,
    subplot_shape = "irregular",  # Quadrants of a circle
    subplot_width = NA,
    subplot_length = NA,
    subplot_radius = NA,
    plot_notes = paste(plot_notes, "subplot is 90deg quadrant of circle"),
    dead_stems_sampled = FALSE,
    catenal_position = case_when(
      catenal_position == "concave bottom slope" ~ "concave_slope",
      catenal_position == "convex upslope" ~ "convex_slope"),
    canopy_cover_method_notes = canopy_cover_method, 
    canopy_cover_method = NA,
    tree_stem = "stem",
    tree_diff = TRUE,
    polygons = FALSE,
    file_plots = "36_southafrica_twine_SUCSES_checkY.xlsx",
    file_stems = "36_southafrica_bushbuck_twine_clean.csv",
    data_ref = NA,
    permanent = FALSE,
    nested_2 = FALSE,
    nested_3 = FALSE, 
    n1_plot_shape = "circle", 
    n1_plot_radius = n2_plot_radius,
    min_height_thresh = n1_min_height_thresh / 100,
    n1_min_height_thresh = n2_min_height_thresh,
    n1_min_diam_thresh = 0,
    min_diam_thresh = n2_min_diam_thresh,
    n2_min_diam_thresh = NA,
    n2_min_height_thresh = NA,
    n2_plot_shape = NA,
    n2_plot_radius = NA
    )

# Check
plots_clean_check <- colValCheck(plots_clean_val, colClass = plotColClass())

plotTableCheck(plots_clean_check)

plots_table <- plotExtraColGen(plots_clean_check)

plots_table$subplot_area <- plots_table$plot_area / 4

# Polygons ----
polys <- polyGen(plots_table[plots_table$polygons == FALSE,])

# Stem cleaning ----

# Rename columns
names(stems) 

stems_clean <- stems %>%
  dplyr::select(-X,
    -plot_id,
    plot_id = plotcode,
    stem_id,
    measurement_date = year,
    subplot_id = subplot1,
    -subplot2,
    longitude = x,
    latitude = y,
    species_orig_local = spp_local,
    species_orig_binom = spp_orig_sci,
    -spp,
    notes_stem = notes_tree,
    diam,
    pom,
    -d_pom_tminus1,
    tree_id = multiple,
    height, 
    crown_x = crown_diam,
    alive,
    stem_status = a_statusF1,
    broken_per_remain = broken_bm_remain,
    broken_height = broken_h,
    stem_mode = death_modeF2,
    diam_method = method_dbhF3,
    -data_mgmtF4,
    height_method = method_hF5,
    -notes_census,
    voucher_id,
    -voucher_collected) %>%
  colGen(., stemColClass())

# Fix values
stems_clean_val <- stems_clean %>%
  mutate(plot_id = gsub("-(0)?(0)?", "_", .$plot_id),
    pom = pom / 100,
    stem_mode = case_when(
      stem_status == "p" ~ "P",
      TRUE ~ NA_character_),
    stem_status = NA,
    diam = case_when(
      diam == 40.9 & plot_id == "SBT_31" ~ 4.9,
      diam == 40.8 & plot_id == "SBT_31" ~ 4.8,
      TRUE ~ diam),
    fpc = case_when(
      diam < 4 | height < 6 ~ 0.1,
      TRUE ~ 1)) %>% 
  idClean(.)

# Check
stems_clean_check <- colValCheck(stems_clean_val, 
  colClass = stemColClass())

stemTableCheck(stems_clean_check)

# Check species names ----
stems_species <- speciesGen(stems_clean_check, 
  return_unknown = TRUE, return_unknown_gnr = TRUE, return_unknown_cjb = TRUE)

lookup <- data.frame(
  original = stems_species,
  corrected = case_when(
    stems_species == "Acacia natalensis" ~ "Fabaceae indet",
    stems_species == "Acaciua caffra" ~ "Senegalia caffra",
    stems_species == "Diospyros mespiliormis" ~ "Diospyros mespiliformis",
    stems_species == "Dodonea angustifolia" ~ "Dodonaea viscosa",
    stems_species == "Gewia caffra" ~ "Grewia caffra",
    stems_species == "Grewia monticolor" ~ "Grewia monticola",
    stems_species == "Pavaetta schumanniana" ~ "Pavetta schumanniana",
    stems_species == "Rhus rehmanniania" ~ "Searsia rehmanniana",
    stems_species == "Sapium intergerrimum" ~ "Sclerocroton integerrimus",
    TRUE ~ NA_character_))

stems_species <- speciesGen(stems_clean_check, ortho = lookup,
  return_unknown = FALSE, return_unknown_gnr = FALSE, return_unknown_cjb = FALSE)

# Add extra columns ----
stems_extra <- stemExtraColGen(stems_species,
  species_name = "species_name_clean")
stems_extra$agb <- agbGen(stems_extra, plots_table, diam = "diam_adj", 
  species_name = "species_name_clean")
stems_extra$subdiv_id <- NA
plots_table$subdiv <- NA

# Check the stem and plot tables together ----
stemPlotTableCheck(stems_extra, plots_table)

# Generate plot level statistics from stem table ----
plots_table_stat <- plotStemColGen(stems_extra, plots_table, species_name = "species_name_clean")

# Export data ----
exportData(file.path(args[2], "twine_sucses"), stems = stems_extra, 
  plots = plots_table_stat, polys = polys, overwrite = !interactive(), 
  files = c("stems.csv", "plots.csv", "points.shp", "polys.shp"))

