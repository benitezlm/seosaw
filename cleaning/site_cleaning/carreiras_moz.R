# Clean Carreiras Mozambique data
# John Godlee (johngodlee@gmail.com)
# 2020-05-15

# Packages
library(dplyr)
library(seosawr)
library(readxl)
library(sf)

source("../functions.R")  

# Import data 
plots <- read_xlsx(file.path(args[1], "carreiras_moz/44_moz_carreiras_checkY.xlsx"), 
  sheet = 2)
stems <- read.csv(file.path(args[1], "carreiras_moz/44_mozambique_lugela_carreiras_clean.csv"))
polys <- st_read(file.path(args[1], "carreiras_moz/Carreiras_mozambique_polygons_circ20m_radius.shp"))

# Plot cleaning ----

# Rename and create columns
plots_clean <- old_plot_col_repl(plots) %>%
  colGen(., plotColClass())

# Fix values 
plots_clean_val <- plots_clean %>%
  mutate(
    plot_radius = plot_radius * 2,
    plot_id = gsub("-(0)?(0)?", "_", .$plot_id), 
    date_metadata = 2017,
    prinv = gsub(",", "", prinv),
    other_authors = "Melo J. B.; Vasconcelos M. J.",
    plot_cluster = plot_id,
    nested_2 = FALSE, 
    nested_3 = FALSE,
    tree_stem = "stem",
    file_plots = "44_moz_carreiras_checkY.xlsx",
    file_stems = "44_mozambique_lugela_carreiras_clean.csv",
    data_ref = NA,
    min_diam_thresh = n1_min_diam_thresh,
    n1_min_diam_thresh = NA,
    permanent = FALSE,
    tags = FALSE,
    sampling_notes = "Diameter measurements with rounded diameter tapes",
    tree_diff = TRUE,
    permanent = FALSE)

plots_clean_check <- colValCheck(plots_clean_val, colClass = plotColClass())

plotTableCheck(plots_clean_check)

plots_table <- plotExtraColGen(plots_clean_check)

# Polygons ----

polys_clean <- polys %>%
  mutate(plot_id = plots_table$plot_id) %>%
  dplyr::select(plot_id)

# Stem cleaning ----

# Rename columns
names(stems)

stems_clean <- stems %>%
  dplyr::select(-X, 
    -plot_id,
    plot_id = plotcode, 
    stem_id,
    measurement_date = year, 
    subplot_id = subplot1, 
    -subplot2,
    x_grid = x, 
    y_grid = y, 
    species_orig_local = spp_local,
    species_orig_binom = spp_orig_sci,
    -spp,
    notes_stem = notes_tree, 
    diam, 
    pom,
    -d_pom_tminus1,
    tree_id = multiple, 
    height, 
    crown_x = crown_diam, 
    alive, 
    stem_status = a_statusF1,
    broken_per_remain = broken_bm_remain,
    broken_height = broken_h,
    stem_mode = death_modeF2,
    diam_method = method_dbhF3,
    -data_mgmtF4,
    height_method = method_hF5,
    -notes_census,
    voucher_id,
    -voucher_collected) %>%
  colGen(., stemColClass())

# Fix values
stems_clean_val <- stems_clean %>%
  mutate(plot_id = gsub("-(0)?(0)?", "_", .$plot_id),
    species_orig_local = as.character(species_orig_local),
    species_orig_local = case_when(
      species_orig_binom == "messassa" ~ species_orig_binom,
      TRUE ~ species_orig_local),
    alive = "A",
    stem_status = NA,
    stem_mode = NA,
    diam_method = "dbh_tape", pom = 1.3) %>% 
  idClean(.)

stems_clean_check <- colValCheck(stems_clean_val, colClass = stemColClass())

stemTableCheck(stems_clean_check)

# Check species names ----
stems_species <- speciesGen(stems_clean_check, 
  return_unknown_gnr = TRUE, return_unknown_cjb = TRUE,
  return_unknown = TRUE)

user_lookup <- data.frame(
  original = stems_species, 
  corrected = case_when(
    stems_species == "Ambligonocarpus andongensis" ~ "Amblygonocarpus andongensis",
    stems_species == "Bechel vai procurar, viagra" ~ "Indet indet",
    stems_species == "Berckemia discolor" ~ "Berchemia discolor",
    stems_species == "Brachystegia boemii" ~ "Brachystegia boehmii",
    stems_species == "Branquierica zanzibarica" ~ "Indet indet",
    stems_species == "Breonadia salicina (mugonha)" ~ "Breonadia salicina",
    stems_species == "Chimenia americana" ~ "Ximenia americana",
    stems_species == "Combretum colinum" ~ "Combretum collinum",
    stems_species == "Crossopterix febrifuga" ~ "Crossopteryx febrifuga",
    stems_species == "Fabaceae indet" ~ "Fabaceae indet",
    stems_species == "Gardenia voliskein" ~ "Gardenia volkensii",
    stems_species == "Lannea scheniforti" ~ "Lannea schweinfurthii",
    stems_species == "Messassa" ~ "Brachystegia spiciformis",
    stems_species == "Nao identificada 1" ~ "Indet indet",
    stems_species == "Pericopisis angolensis" ~ "Pericopsis angolensis",
    stems_species == "Piliostgma thonningii" ~ "Piliostigma thonningii",
    stems_species == "Pseudolachnostrylis maprounefolia" ~ "Pseudolachnostylis maprouneifolia",
    stems_species == "Pterocarpus rotundifolia" ~ "Pterocarpus rotundifolius",
    stems_species == "Securinega indet" ~ "Securinega indet",
    stems_species == "Sterculia ???" ~ "Sterculia indet",
    TRUE ~ NA_character_))

stems_species <- speciesGen(stems_clean_check, 
  return_unknown_gnr = FALSE, return_unknown_cjb = FALSE,
  return_unknown = FALSE, ortho = user_lookup)

# Add extra columns ----
stems_extra <- stemExtraColGen(stems_species)
stems_extra$agb <- agbGen(stems_extra, plots_table, diam = "diam_adj", 
  species_name = "species_name_clean")
stems_extra$subdiv_id <- NA
plots_table$subdiv <- NA

# Check the stem and plot tables together ----
stemPlotTableCheck(stems_extra, plots_table)

# Generate plot level statistics from stem table ----
plots_table_stat <- plotStemColGen(stems_extra, plots_table, 
  species_name = "species_name_clean")

# Export data ----
exportData(file.path(args[2], "carreiras_moz"), stems = stems_extra, 
  plots = plots_table_stat, polys = polys_clean, overwrite = !interactive(), 
  files = c("stems.csv", "plots.csv", "points.shp", "polys.shp"))
