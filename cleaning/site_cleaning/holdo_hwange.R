# Clean Holdo Hwange data
# John Godlee (johngodlee@gmail.com)
# 2020-05-15

# Packages
library(dplyr)
library(seosawr)
library(readxl)

source("../functions.R")  

# Import data
plots <- read_xlsx(file.path(args[1], "holdo_hwange/10_zim_holdo_hwange_checkY.xlsx"),
  sheet = 2)
stems <- read.csv(file.path(args[1], "holdo_hwange/10_zim_holdo_hwange_clean_manualEditCR.csv"))

# Plot cleaning ----

# Rename and create columns
plots_clean <- old_plot_col_repl(plots) %>%
  colGen(., plotColClass())

# Fix values
plots_clean_val <- plots_clean %>%
  mutate(plot_id = gsub("-(0)?(0)?", "_", .$plot_id),
    date_metadata = 2017,
    plot_cluster = plot_id,
    nested_1 = TRUE,
    min_diam_thresh = 5,
    n1_plot_shape = "rectangle",
    n1_plot_length = 50,
    n1_plot_width = 4,
    n1_min_diam_thresh = 1,
    n2_min_diam_thresh = NA,
    n2_plot_length = NA,
    n2_plot_width = NA,
    n2_plot_shape = NA,
    nested_2 = FALSE, 
    nested_3 = FALSE, 
    all_dates = 2002,
    pa = TRUE, 
    pa_type = "NP", 
    pa_name = "Hwange",
    wdpa_id = 1991,
    canopy_cover = NA, 
    canopy_cover_method = NA, 
    catenal_position = "flat",
    termites = NA,
    elevation = NA,
    tree_stem = "stem",
    tree_diff = TRUE,
    polygons = FALSE,
    permanent = TRUE,
    prinv = "Holdo R.",
    file_plots = "10_zim_holdo_hwange_checkY.xlsx",
    file_stems = "10_zim_holdo_hwange_clean_manualEditCR.csv",
    elephant_density = gsub(" elephants/km2", "", .$elephant_density),
    plot_notes = "termites 5.8 km^-2",
    data_ref = "https://doi.org/10.1007/s11258-005-2796-4")

# Check
plots_out <- colValCheck(plots_clean_val, colClass = plotColClass())
  
plotTableCheck(plots_out)

plots_table <- plotExtraColGen(plots_out)

# Polygons
polys <- polyGen(plots_table[plots_table$polygons == FALSE,])

# Stem cleaning ----

# Rename columns
stems_clean <- stems %>%
  dplyr::select(-X, 
    -plot_id,
    plot_id = plotcode, 
    stem_id,
    measurement_date = year, 
    tag_id, 
    subplot_id = subplot1,
    -subplot2, 
    x_grid = x,
    y_grid = y,
    species_orig_local = spp_local,
    species_orig_binom = spp_orig_sci,
    -spp,
    notes_stem = notes_tree,
    diam,
    pom,
    -d_pom_tminus1,
    tree_id = multiple,
    height,
    crown_x = crown_diam,
    alive,
    stem_status = a_statusF1,
    broken_per_remain = broken_bm_remain,
    broken_height = broken_h,
    stem_mode = death_modeF2,
    diam_method = method_dbhF3,
    -data_mgmtF4,
    height_method = method_hF5,
    voucher_id,
    -voucher_collected) %>%
  colGen(., stemColClass())

# Fix values
stems_clean_val <- stems_clean %>%
  mutate(plot_id = gsub("-(0)?(0)?", "_", .$plot_id),
    tag_id = case_when(
      tag_id == "NO TAG" ~ NA_character_,
      TRUE ~ tag_id),
    pom = 0.3, 
    broken_height = case_when(
      broken_height == "?" ~ NA_character_,
      TRUE ~ broken_height),
    standing_fallen = case_when(
      grepl("topple", notes_stem, ignore.case = TRUE) ~ "F",
      TRUE ~ as.character(standing_fallen)),
    fpc = case_when(
      diam < 5 ~ 0.2,
      diam >= 5 ~ 1),
  stem_mode = NA,
  stem_status = NA) %>%
  idClean(.)

# Check
stems_clean_check <- colValCheck(stems_clean_val, colClass = stemColClass())

stemTableCheck(stems_clean_check)

# Check species names ----
stems_species <- speciesGen(stems_clean_check, 
  return_unknown = TRUE, return_unknown_gnr = TRUE, return_unknown_cjb = TRUE)

lookup_table <- data.frame(
  original = stems_species,
  corrected = case_when(
    stems_species == "?" ~ "Indet indet",
    stems_species == "Acfl" ~ "Senegalia cinerea",
    stems_species == "Acni" ~ "Senegalia nigrescens",
    stems_species == "Aman" ~ "Amblygonocarpus andongensis",
    stems_species == "Bama" ~ "Baphia massaiensis",
    stems_species == "Bape" ~ "Bauhinia petersiana",
    stems_species == "Bapl" ~ "Baikiaea plurijuga",
    stems_species == "Brbo" ~ "Brachystegia boehmii",
    stems_species == "Brsp" ~ "Brachystegia spiciformis",
    stems_species == "Brsp?" ~ "Brachystegia spiciformis",
    stems_species == "Buaf" ~ "Burkea africana",
    stems_species == "Caab" ~ "Cassia abbreviata",
    stems_species == "Cahu" ~ "Psydrax livida",
    stems_species == "Coaf" ~ "Commiphora africana",
    stems_species == "Coap" ~ "Combretum apiculatum",
    stems_species == "Coap?" ~ "Combretum apiculatum",
    stems_species == "Coco" ~ "Combretum collinum",
    stems_species == "Cohe" ~ "Combretum hereroense",
    stems_species == "Como" ~ "Colophospermum mopane",
    stems_species == "Cops" ~ "Combretum psidioides",
    stems_species == "Cops?" ~ "Combretum psidioides",
    stems_species == "Coze" ~ "Combretum zeyheri",
    stems_species == "Coze?" ~ "Combretum zeyheri",
    stems_species == "Dico" ~ "Diplorhynchus condylocarpon",
    stems_species == "Eraf" ~ "Erythrophleum africanum",
    stems_species == "Eudi" ~ "Euclea divinorum",
    stems_species == "Grfl" ~ "Grewia flavescens",
    stems_species == "Guco" ~ "Guibourtia coleosperma",
    stems_species == "Jugl" ~ "Julbernardia globiflora",
    stems_species == "Lone" ~ "Philenoptera nelsii",
    stems_species == "Maac" ~ "Markhamia zanzibarica",
    stems_species == "Ocpu" ~ "Ochna pulchra",
    stems_species == "Peaf" ~ "Peltophorum africanum",
    stems_species == "Psma" ~ "Pseudolachnostylis maprouneifolia",
    stems_species == "Ptan" ~ "Pterocarpus angolensis",
    stems_species == "Rhte" ~ "Searsia tenuinervis",
    stems_species == "Sevi?" ~ "Flueggea virosa",
    stems_species == "Tebr" ~ "Terminalia brachystemma",
    stems_species == "Tese" ~ "Terminalia sericea",
    stems_species == "Tese?" ~ "Terminalia sericea",
    stems_species == "Xisp" ~ "Ximenia americana",
    TRUE ~ NA_character_))

stems_species <- speciesGen(stems_clean_check, ortho = lookup_table,
  return_unknown = FALSE, return_unknown_gnr = FALSE, return_unknown_cjb = FALSE)

# Add extra columns ----
stems_extra <- stemExtraColGen(stems_species, 
  species_name = "species_name_clean")
stems_extra$agb <- agbGen(stems_extra, plots_table, diam = "diam_adj", 
  species_name = "species_name_clean")
stems_extra$subdiv_id <- NA
plots_table$subdiv <- NA

# Check the stem and plot tables together ----
stemPlotTableCheck(stems_extra, plots_table)

# Generate plot level statistics from stem table ----
plots_table_stat <- plotStemColGen(stems_extra, plots_table, species_name = "species_name_clean")

# Export data ----
exportData(file.path(args[2], "holdo_hwange"), stems = stems_extra, 
  plots = plots_table_stat, polys = polys, overwrite = !interactive(), 
  files = c("stems.csv", "plots.csv", "points.shp", "polys.shp"))

