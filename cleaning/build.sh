#!/usr/bin/env sh

# Check that arguments provided
if [ $# -ne 7 ]; then
	printf "Must supply five arguments:\n  [1] SEOSAW raw data directory\n  [2] SEOSAW clean data directory\n  [3] SEOSAW data out directory\n  [4] SEOSAW R package data directory\n  [5] SEOSAW raw spatial data directory\n  [6] Version of this dataset, e.g. v2.10\n  [7] SEOSAW table columns directory\n  [8] SEOSAW data entry template directory\n"
    exit 1
fi

# Clean each site
for i in site_cleaning/*.R ; do 
	Rscript $i $1 $2 & 
done

# Compile all sites
Rscript compile_all.R $2 $3 $4 $6 $7 $8

# Clusters
Rscript clusters.R $3

# Extract spatial data
Rscript spatial_extract.R $5 $3 $7

# Data quality
Rscript qual.R $3
