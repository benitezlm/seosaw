This directory contains RMarkdown report templates to help visualise the SEOSAW dataset in different and accessible ways. Each report comes with it's own build script which take parameters to build reports dynamically. For example, to compile a report which summarises a particular SEOSAW dataset ID:

```r
cd dataset_report

Rscript report_compile.R DATASET_ID SEOSAW_DATA_DIR
```

