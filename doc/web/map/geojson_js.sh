#!/usr/bin/env bash

Rscript plots_geojson.R $1
Rscript region_geojson.R $1

# region.geojson -> region.js
## Add line at start
gsed -i '1s/^/var region = [/' region.geojson

## Add line at end
gsed -i '$s/$/]/' region.geojson

## Move temp file to permanent
mv region.geojson ../out/region.js

# plots.geojson -> plots.js
touch plots.geojson.tmp

## Remove first 4 lines
sed -e '1,4d' < plots.geojson > plots.geojson.tmp

## Add line at start
gsed -i '1s/^/var plots = [/' plots.geojson.tmp

# Remove last line
gsed -i '$ d' plots.geojson.tmp

mv plots.geojson.tmp ../out/plots.js
rm plots.geojson 



