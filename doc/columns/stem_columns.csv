﻿column,class,description,units,example,filled_by_data_contributor,generated_by
plot_id,chr,"A standardised method of identifying plots. For example, the first plot in a set of plots installed in Angola (A), in Bicuar National Park (B), by John Godlee (G), would be referred to as ABG_1.",NA,ABG_1,N,SEOSAW
subplot_id,chr,"If the plot is divided into multiple parcels, but not ""nested"", i.e. sampling effort is equal across all parcels, the identifier of those parcels",NA,A,Y,NA
tree_id,chr,"The identifier for a tree within a plot, ideally a number increasing from 1 to the number of trees in a plot, but this is not strict",NA,45,Y,NA
stem_id,chr,"The identifier for a stem within a tree, ideally a number increasing from 1 to the number of stems in a tree, but this is not strict",NA,6,Y,NA
tag_id,chr,"The code of the tag attached to the tree or stem in the field. If tags were not used, leave this column blank",NA,BP094,Y,NA
measurement_date,chr,The year the measurement was taken.,NA,1993,Y,NA
species_orig_binom,chr,"The binomial name of the tree species, as provided by the data originator",NA,Burkea africana,Y,NA
species_orig_local,chr,"The local name of the tree species/morphospecies, as provided by the data originator",NA,mumwei,Y,NA
notes_stem,chr,Miscellaneous notes related to the tree or stem,NA,Leaning around 45 degrees and with a beehive in the fluted trunk,Y,NA
diam,num,Stem diameter measurement in cm,cm,6.4,Y,NA
pom,num,"The height at which diam was taken, in metres to one decimal place (nearest 10 cm)",m,1.3,Y,NA
x_grid,num,The x coordinate of the stem within the plot if a grid method was used. In metres to nearest 10 cm,m,54.2,Y,NA
y_grid,num,The y coordinate of the stem within the plot if a grid method was used. In metres to nearest 10 cm,m,35.6,Y,NA
latitude,num,The latitude coordinate of the stem within the plot if a GPS method was used. WGS84 EPSG4326,decimal degrees,-50.425622,Y,NA
longitude,num,The longitude coordinate of the stem within the plot if a GPS method was used. WGS84 EPSG4326,decimal degrees,30.446453,Y,NA
angle,num,"If `plot_shape` == ""circle"", and angular stem coordinates were used, the angle (0 = North) direction of the stem from the plot centre, in degrees (0-360)",degrees,43.2,Y,NA
distance,num,"If `plot_shape` == ""circle"", and angular stem coordinates were used, the distance of the stem from the plot centre. In metres to the nearest 10 cm",m,3.5,Y,NA
height,num,"The height of the tree, in metres to one decimal place (nearest 10 cm)",m,3.5,Y,NA
bole_height,num,The height from the ground to the base of the lowest branch also called 'clear length',m,6.3,Y,NA
crown_x,num,"If two crown diameters are taken perpendicular to one another, the first of these measurements. If one crown diameter was taken, that crown diameter, in metres to one decimal place (nearest 10 cm).",m,5.2,Y,NA
crown_y,num,"If two crown diameters are taken perpendicular to one another, the second of these measurements. If one crown diameter was taken, this remains blank, in metres to one decimal place (nearest 10 cm).",m,3.5,Y,NA
recruit,logi,"Is the stem a new recruit since the last census? If this is the first census, leave blank.",NA,TRUE,Y,NA
on_termites,logi,Is the stem located on a termite mound?,NA,FALSE,Y,NA
lightning,logi,Has the stem been struck by lightning?,NA,TRUE,Y,NA
canopy_climbers,logi,Does the stem have other climbing plants in its canopy?,NA,FALSE,Y,NA
stem_stranglers,logi,Does the stem have strangling plants on it?,NA,TRUE,Y,NA
stump,logi,Is the stem a stump? I.e. has it been cut or broken lower than the POM?,NA,TRUE,Y,NA
liana,logi,"Is the stem a liana, i.e. a woody stem that is not self-supporting and uses other trees to grow towards the canopy",NA,FALSE,Y,NA
alive,fct,"Is the stem alive or dead. Possible values: ""a"" (alive), ""d"" (dead)",NA,A,Y,NA
stem_status,fct,"What is the growth status of the stem. Possible values are: ""r"" if the stem is topkilled and resprouting, ""t"" if the stem is topkilled with no sign of resprouting, ""d"" if the stem is completely dead (i.e. base appears dead), NA if the stem is alive with no abnormal growth status.",NA,R,Y,NA
standing_fallen,fct,"Is the stem standing, or fallen? Possible values are ""s"" if the stem is standing, ""f"" if the stem is fallen at a clearly unnatural angle.",NA,F,Y,NA
broken_per_remain,num,"If the stem is broken, approximately how much is remaining? As a decimal, e.g. 60% of stem remaining = 0.6, to one decimal place (nearest 10%)",proportion,0.2,Y,NA
broken_height,num,"If the stem is broken, at what height is it broken. In metres to one decimal place (nearest 10 cm)",m,4.5,Y,NA
stem_mode,fct,"What position is the stem in? Possible values are: ""u"" if uprooted, ""p"" if snapped, ""s"" if standing, ""v"" if vanished (location of tree found), ""q"" if unknown, ""x"" if location of tree not found due to problems, poor maps, etc.",NA,P,Y,NA
damage_cause,fct,"If the stem is damaged, what caused it? Possible values are: ""n"" if neighbouring tree, ""e"" if elephants, ""f"" if fire, ""h"" if humans (.e.g. cut, ringbarked, broken or beehive), ""l"" if lightning, ""m"" if termites, ""w"" if wind, ""q"" if unknown",NA,N,Y,NA
damage_cause_human,fct,"If the stem is damaged, and damage_cause = ""h"". What was the specific method of damage by humans. Possible values are: ""chainsaw"" if cut by a chainsaw, ""axe"" if cut by an axe, ""machete"" if cut by machete, ""ringbark"" if ringbarked, ""beehive"" if the bark was removed to access a beehive, ""other"".",NA,chainsaw,Y,NA
cambium,int,"The proportion of trunk surface area with exposed vascular cambium, between the ground and 2 m.",proportion,0.22,Y,NA
bracket_fungi,logi,Does the stem have bracket fungi?,NA,TRUE,Y,NA
decay,num,"What level of decay is the stem? Scale runs from 1 (no decay) to 5 (very decayed). Follow the ""Decay class"" table in the SEOSAW protocol. To 0.5 of a decay level (e.g. 1.5, 1, 3.5)",NA,4.5,Y,NA
diam_method,fct,"What method was used to derive the stem diameter measurement? Possible values are: ""dbh_tape"" if a tape measure with a diameter conversion scale was used, ""tape"" if a normal tape measure was used and the circumference converted to diameter, ""caliper_1"" if a single caliper measurement was used, ""caliper_2"" if two caliper measurements at 90 degrees with the mean taken, ""other"".",NA,dbh_tape,Y,NA
height_method,fct,"What method was used to derive the height measurement? Possible values are: ""eye"" if estimated by eye, ""rangefinder"" if estimated using laser or ultrasonic apparatus with an electronic tilt sensor e.g. Nikon Forestry Pro, ""clinometer"" if estimated using a mechanical Clinometer, ""hyposometer"" if estimated using a laser vertex hypsometer from below the tree using last return, ""direct"" if estimated by climbing the tree or adjacent tower and measuring the height with tape or stick, ""other""",NA,eye,Y,NA
voucher_id,chr,"If a herbarium voucher has been collected, what is the code to identify it? 1, Wedza_01, MHR-04b are all valid. No spaces please.",NA,KEW_FK_42,Y,NA
fpc,num,"Fractional Probability of Inclusion. If plots are nested with varying sampling effort within plots for stems of different size, the probability of a stem this size occurring in the whole plot.",proportion,0.35,Y,NA
measurement_id,chr,A standardised method of identifying measurements. A composite of `plot_id`_`tree_id`_`stem_id`_`measurement_date`,NA,ABG_1_40_3_1990,N,idClean()
crown_area,num,"Area of the tree crown, measured as an ellipse using either one or two crown diameter measurements, if present. In m^2",m^2,4.5,N,stemExtraColGen()
agb,num,"Aboveground biomass of stem, in tonnes of dry matter",t DM,0.093,N,agbGen()
wood_density,num,"Wood density of stem, in g cm^-3",g cm^-3,0.042,N,woodDensityQuery()
ba,num,"Basal area of stem, in m^2 ",m^2,0.46,N,stemExtraColGen()
diam_adj,num,Adjusted stem diameter at 1.3 m height accounting for tree taper,cm,5.3,N,stemExtraColGen()
species_name_clean,chr,Cleaned species name after taxize checks and synonymy corrections,NA,Burkea africana,N,speciesGen()
genus_clean,chr,"Cleaned genus name, taken from `species_name_clean`",NA,Burkea,N,speciesGen()
species_clean,chr,"Cleaned species epithet, taken from `species_name_clean`",NA,africana,N,speciesGen()
subspecies_clean,chr,"Cleaned subspecies epithet, taken from `species_name_clean`",NA,africana,N,speciesGen()
confer_clean,chr,"Cleaned cf. part of species epithet, taken from `species_name_clean`",NA,polyantha,N,speciesGen()
variety_clean,chr,"Cleaned var. part of species epithet, taken from `species_name_clean`",NA,micrantha,N,speciesGen()
family_gnr,chr,Matched family name using taxize::gnr_resolve(),NA,Fabaceae,N,speciesGen()
name_gnr,chr,Name matched during the GNR resolve,NA,Burkea africana,N,speciesGen()
taxon_rank_gnr,chr,"All rank names of classification from GNR resolve, separated by ""|""",NA,|superkingdom|kingdom|phylum|subphylum|||||class|||||||order|family|subfamily||||tribe|genus|species,N,speciesGen()
taxon_gnr,chr,"All ranks of classification from GNR resolve, separated by ""|""",NA,|Eukaryota|Viridiplantae|Streptophyta|Streptophytina|Embryophyta|Tracheophyta|Euphyllophyta|Spermatophyta|Magnoliopsida|Mesangiospermae||Gunneridae|Pentapetalae|||Fabales|Fabaceae|Papilionoideae||||Millettieae|Millettia|Millettia makondensis,N,speciesGen()
base_rank_gnr,chr,The lowest rank identified in GNR resolve,NA,species,N,speciesGen()
name_cjb,chr,Full species name with authority matched with the CJB African Plants database,NA,Burkea africana Host.,N,speciesGen()
url_cjb,chr,URL of species record matched with the CJB African Plants database,NA,https://www.ville-ge.ch/…,N,speciesGen()
proj_SA_cjb,logi,Is the species part of the CJB Southern African Area?,NA,TRUE,N,speciesGen()
proj_TA_cjb,logi,Is the species part of the CJB Tropical African Area?,NA,FALSE,N,speciesGen()
proj_NA_cjb,logi,Is the species part of the CJB Northern African Area?,NA,TRUE,N,speciesGen()
proj_MA_cjb,logi,Is the species part of the CJB Madagascar Area?,NA,FALSE,N,speciesGen()
subdiv_id,chr,"If the plot has been divided into smaller 1 ha subdivisions, the names of 1 ha plot subdivisions to which the stem belongs.",NA,ABG_1_S4,N,largePlotSplit()