#!/usr/bin/env sh

drawio() {
	/Applications/draw.io.app/Contents/MacOS/./draw.io --crop -x -o $2 $1 \;
}

drawio drawio/subplot.drawio img/subplot.pdf 
drawio drawio/nested.drawio img/nested.pdf 
drawio drawio/clean_flow.drawio img/clean_flow.pdf 

pdftoppm -png img/subplot.pdf > img/subplot.png
pdftoppm -png img/nested.pdf > img/nested.png
pdftoppm -png img/clean_flow.pdf > img/clean_flow.png

Rscript --verbose -e "require(knitr); knitr::knit('seosaw_dataset_manual.Rnw')"

latexmk -pdf -pdflatex="pdflatex -interaction=nonstopmode" -bibtex seosaw_dataset_manual.tex

latexmk -pdf -pdflatex="pdflatex -interaction=nonstopmode" -bibtex seosaw_dataset_manual.tex

latexmk -c

rm seosaw_dataset_manual.tex
