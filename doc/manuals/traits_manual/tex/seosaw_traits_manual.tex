\documentclass[11pt,a4paper]{article}

\usepackage{mynotes}
\usepackage{amssymb}
\usepackage{array}

\newcommand{\C}{\checkmark}

\addbibresource{traits_manual.bib}

\newcommand\menu[1]{\texttt{\color{blue}#1}}
\newcommand\file[1]{\texttt{\underline{#1}}}

\title{Characterising plant traits in dry tropical woodlands}
\author{John L. Godlee\textsuperscript{1}, A. Carla Staver\textsuperscript{2}, Sally Archibald\textsuperscript{3}}
\date{\today{}}

\begin{document}

\maketitle{}

\textsuperscript{1}School of GeoSciences, University of Edinburgh, Edinburgh, EH9 3FF, United Kingdom. \\
\textsuperscript{2}Department of Ecology and Evolutionary Biology, Princeton University, Princeton, NJ 08544, USA. \\
\textsuperscript{3}School of Animal, Plant and Environmental Sciences, University of the Witwatersrand, Johannesburg, South Africa.

\tableofcontents{}

\section{Introduction}

This guide outlines a number of methods for measuring plant traits of trees in dry tropical woodlands. While \citet{SEOSAW} does not keep a formal database of trait data, this guide has been written in response to various collaborators seeking advice on collecting trait data during construction of SEOSAW plots. This guide strives to provide accessible and low-tech methods for collecting trait data, rather than state of the art high-precision methods. At the end of each section, we recommend an optimal protocol for trait measurement, based on a balance between labour, skill required, cost, accuracy and precision. Despite our suggestion of an optimal protocol however, trait data collection should first and foremost suit the research goals of the researcher. This guide does not document every plant trait available for measurement, merely some which are commonly collected and expected to be informative for dry tropical woodlands. For a guide which covers many more traits and focusses more on fine-scale trait collection, see \citet{Wigley2020}. For most traits, a number of different methods are described, all of which are equally valid under different circumstances. The reader is encouraged to critically appraise each method regarding its applicability for their own work. 

Plant traits constitute the morphological, anatomical, physiological, biochemical and phenological characteristics of plants \citep{TRY}. Variation in plant traits determines how individuals respond to their immediate environment, defines the multidimensional niche space a species occupies, how that species influences ecosystem processes, and thus provides the link between species diversity and ecosystem function \citep{Butler2017, Madani2018}. Scaling up from individual plant traits, community-level traits such as woodland canopy cover provide valuable information on ecosystem structure, competition effects, and community-level responses to environmental perturbations which cannot be adequately represented by simply aggregating traits of individual plants in a community \citep{He2019}. Moving away from community ecology and plot studies, plant traits are routinely used in studies covering trait evolution \citep{Donovan2011}, plant migration and invasion ecology \citep{Drenovsky2012}, and climate change response \citep{Heilmeier2019}. Plant trait datasets are highly valuable assets for research, providing a multitude of research outcomes when analysed in different ways and combined with other data. While collecting plant traits may appear laborious, the long term benefit may far outweigh the initial effort of data collection.

Conducting accurate and precise ecological plant trait measurements requires a balance between financial expense, time spent, and skill required. While advances in technology have allowed extremely precise collection of many plant traits, the initial financial outlay for equipment precludes their use in research for many \citep{Allan2018}. While this guide mentions multiple methods for measuring plant traits, the focus is on providing low cost methods that should be easily accessible for all researchers operating in the SEOSAW region, regardless of career level or location of host institution. 

A high precision of plant trait measurements is not always required. In lieu of highly precise measurements a representative sampling design and a knowledge of the expected underlying distribution of data can suffice. The random error of plant trait values inherent within a particular species or population can far outweigh the need for precise measurements. The concern should instead be for accurate measurements, which avoid systematic error. In order to avoid undue labour, while at the same time ensuring data is representative of the ecological community, an appropriate sampling strategy should be thoroughly considered before fieldwork begins. Throughout this guide we suggest pragmatic sampling strategies for different traits and plot layouts.

In southern African woodlands particularly, there is a distinct dearth of plant trait data, compared with other regions around the world. The TRY global trait database \citep{TRY} records trait observations at a density of 0.042 km\textsuperscript{-2} in Europe, 0.0076 km\textsuperscript{-2} in North America, and 0.0057 km\textsuperscript{-2} globally, but within the SEOSAW region the density is only 0.0012 km\textsuperscript{-2}. Collection of plant traits in southern African woodlands would certainly benefit our understanding of environmental growth limitations, the effects of disturbance by fire and herbivory, community trait response to climate change, delineation of vegetation types, woody encroachment patterns, and many other pertinent questions in this understudied region.



\section{Leaf traits}

\subsection{Specific Leaf Area / Leaf Mass per Area}

\begin{equation}
	\begin{gathered}
		\text{SLA} = A / m_{d} \\
		\text{LMA} = 1 / \text{SLA}
	\end{gathered}
\end{equation}

Specific leaf area (SLA) is a measure of leaf area ($A$) per unit of dried leaf mass ($m_{d}$). Leaf mass per area (LMA) is the reciprocal of SLA. Leaf area is measured as the area of one side of the leaf, measured fresh. 

SLA can vary substantially on how much sunlight a leaf receives. To standardize this, always measure SLA on leaves that receive full sun. This should be easy in a savanna or miombo-type system, but can be challenging in a closed-canopy forest. Make sure to select fully expanded and hardened, photosynthetically active leaves from adult trees. Avoid damaged leaves wherever possible. A set of clippers (secateurs) or even a sharp knife will be enough to collect leaves. A telescopic pruning saw or branch cutter can be useful for larger trees or in closed-canopy forests where sunlit leaves are out of reach. Remove leaves at the base of the petiole. Remember to consider if the leaf is a compound leaf with individual leaflets. If the leaf is a compound leaf, retain all of the rachis for measurements of SLA.

Leaf area should be measured as soon as possible after the leaf is collected. This is especially important for some fine-leaved species, where the leaves roll up when picked and are very difficult to unfold. If you must however, you can store leaves in a plastic bag, pressed flat in a notebook, or better yet pressed in a herbarium press, to be measured in the lab later. Leaf area should be measured within 24 hours of sample collection, at the most.

The easiest way to measure leaf area is to use a flatbed scanner attached to a laptop. Lay leaves out flat so that they are not overlapping, and include something of known size for scale in the image, a ruler is best. You can cut up leaves if they won't lie flat. Scan samples in colour against a white background. Alternatively, use a camera to take a photo against a white background such as a piece of paper. Use an overhead transparency or piece of transparent perspex to hold leaves flat against the paper. Ensure the camera lens is perpendicular to the photo to avoid distortion. Most researchers use the free ImageJ software to measure leaf area \citep{Schneider2012}. 

You should measure \textapprox{}5-10 leaves per tree, taken from different branches, or more if the leaves are very small, in order to estimate the mean SLA of a single tree. See \autoref{ldmc} for more information on measuring dry leaf mass ($m_{d}$). 


\subsection{Leaf Dry Matter Content}
\label{ldmc}

\begin{equation}
	\text{LDMC} = m_{d} / m_{f}
\end{equation}

Leaf dry matter content (LDMC) is a measure of how much water a leaf contains. Calculating LDMC relies on measuring both the fresh wet mass ($m_{f}$) and dry mass ($m_{d}$) of the leaf. As with leaf area measurements, take 5-10 leaves per tree, and if the tree has compound leaves, retain all of the rachis in measurements.

First, measure the fresh weight of leaves to the nearest 0.01 g. Make sure the leaves don't have any water droplets on their outside; blot them off with a tissue if they do. Again, the sooner you can do this, the better, but you can store the leaves in a plastic bag until the end of the day to weigh if necessary.

After measuring fresh weight, put leaves into a paper bag. It can be useful to use a paper bag that you have already weighed, as some leaves disintegrate when they are dried, however, even dry paper bags absorb some water from the air, so you will need to pre-dry your bags before you pre-weigh them. Dry leaves at a maximum temperature of 60\si{\degree}C for four days. Temperature is important because some forms of nitrogen volatilize at higher temperatures \citep{Babu2018}. Weigh samples immediately after taking them out of the oven, to the nearest 0.01 g. Samples can be stored dry, but absorb water from the air, and so must be weighed soon after drying. 

\subsection{Leaf thickness}

Leaf thickness is not straightforward to measure. There is wide variation in leaf morphology, and wide variation within individual leaves, in addition to leaf thickness being a relatively small dimension (sometimes <100 $\mu$m) \citep{Vile2005}. Leaf thickness should be measured on fresh leaves using a micrometer, accurate to at least 0.01 mm. Leaf thickness should be measured on at least three areas of the leaf, avoiding necrotic areas, veins and the leaf mid-rib.

\subsection{Leaf Carbon, Nitrogen, Phosphorous}

Leaves to be saved for chemical analysis should be fully dried and stored in a dry stable environment until they can be analysed in a lab. Storing leaves in paper envelopes in Silica gel filled plastic bags is a good option.

\begin{framed}
{\large{\textbf{SEOSAW recommended sampling strategy}}}

For each plot, gather 10 leaves from up to 20 individuals per species. Avoid sick or dying trees. Try to choose trees which span the range of tree sizes for that species. For each of these leaves, measure SLA, LDMC, leaf thickness (3x per leaf), leaf carbon, nitrogen, and phosphorous content.

\end{framed}


\section{Wood and bark}

\subsection{Bark thickness}

Bark thickness should be measured at a height of 35$\pm$5 cm on the main stem of the tree. Remove a plug of bark with a shoemakers punch \textapprox{}1.5 cm in diameter. Choose an area of bark free from abnormalities. If a shoemaker's punch cannot be found, use an increment borer (a specialised tool for extracting wood), machete, or even a knife and hammer. In any case, make sure all the bark is extracted. Often there is more bark than you think, but the division between bark and wood should be clear. Once all the bark is extracted, measure the thickness with calipers in mm to the nearest 0.1 mm. For savanna trees with thick fire-proof bark, it can be useful to detach the outer bark and then re-assemble the plug afterwards, since the bark does not hold together well. Repeat the measurement at 2-4 points around the circumference of the tree. Use cardinal directions to avoid bias, but make sure to avoid any dead parts of the tree.

\subsection{Wood density}

Wood density should be measured on the main stem of the tree where possible, with all bark removed. One wood sample per individual tree is sufficient. Use an increment borer to remove a plug from the stem, or cut a wedge out of the tree's stem. Be aware however, that a large wedge removed from the stem can leave the tree vulnerable to invertebrates, fire, and fungal infection \citep{Wigley2019, Schoonenberg2003}.

Sometimes collecting a wood sample from the main stem is not possible. In those cases, collect a piece of branch of with a minimum stem diameter of 1 cm and 5-10 cm long. Again, remove the bark. Make a note of where the wood sample was taken from. Branches are less dense than the main stem of a tree by a factor of \textapprox{}1.4.

To calculate wood density, measure the weight of the sample and its volume. Wood density should be recorded in g cm\textsuperscript{-3}.

Your sample should be weighed after drying in a drying oven at 100-105\si{\degree}C for one to three days (maybe more in the case of large samples). The oven temperature is important; wood binds some water quite tightly, and only fully dries at temperatures above 100\si{\degree}C. You will know the sample is dry when the weight of the sample stops decreasing with additional drying. Record the weight of your sample to the nearest 0.01 g.

Record the volume of your wood sample to the nearest 0.1 cm\textsuperscript{3}. If you collected a cylinder of wood, you can calculate its volume using calipers to measure diameter ($D$) at 3-5 points along the length of the sample, and length ($l$), using the following equation:

\begin{equation}
	V = \pi (D/2)^2 l
\end{equation}

If you do not have a cylinder, measure the volume via water displacement. Partially fill a graduated cylinder with water. Coat the sample in sticky tape or a very thin layer of wax to prevent it soaking up water. Submerge the sample in water and measure the increase in the volume of water in mL, which are equivalent to cm\textsuperscript{3}. If you do not have a graduated cylinder, fill a container with water to the brim. Weigh the full container. Submerge the sample fully, letting water spill out over the edge of the container. Remove the sample and weight the partially empty container. The weight of water lost (i.e. full weight minus partially empty weight) in g will equal the volume of the sample in cm\textsuperscript{3}. Wood volume measurements are not time sensitive, so it is worth waiting until you have access to a lab to do them accurately. 

Wood density can then be calculated as:

\begin{equation}
	\rho = m_{d} / V
\end{equation}

\begin{framed}
{\large{\textbf{SEOSAW recommended sampling strategy}}}

For each plot, gather four wood cores and estimate bark thickness at \textapprox{}35 cm from the main (largest) stem of at least three individuals per species. 

\end{framed}

\section{Tree dimensions}

\begin{figure}[H]
\centering
	\includegraphics[width=0.5\textwidth]{dim.drawio}
	\caption{Schematic diagram of common tree physiognomic traits. Note that canopy diameter is normally measured twice at perpendicular angles.}
	\label{dim}
\end{figure}

\subsection{Trunk diameter}

Trunk diameter is the most common tree trait measured in woodland plots. It is commonly standardised as the Diameter at Breast Height (DBH), which measures the trunk diameter at 1.3 m above the ground. The height of this measurement can and should be altered if there are trunk abnormalities at 1.3 m which would give an unrepresentative measurement of the trunk diameter. A PVC pipe cut to a height of 2 m and marked in 5 cm increments with 5 cm wide duct tape makes a useful apparatus to rapidly measure the height of trunk diameter measurements.

\subsection{Tree height}

Tree height is deceptively difficult to measure, though there are multiple methods, each of which are best used in different situations. For small trees, a long straight pole is suitable. Attach a long tape measure to the top end of the pole, then raise the pole vertically to the height of the tree, extend the tape measure to the ground and take the height measurement. A telescopic pole can be made with lengths of PVC pipe which fit snugly inside each other, with holes drilled at intervals for tent pegs to set the height. Alternatively, if vertical cuts are made in the end of the exterior section of each pipe join, jubilee clips (hose clips) can be used to to tighten the exterior pipe against the interior, creating friction and setting the length of the pole.

Above a certain height, poles become unwieldy. Very long poles may flex depending on what they are made of, and can be difficult to balance. Additionally, it can become difficult to identify where the top of the tree is while standing below it with a pole. It is recommended to have two researchers present, one holding the pole below the tree and the other standing a distance away so that they may see when the pole is raised to the correct height.  

Laser rangefinders and hypsometers offer rapid tree height estimation, but be aware that the measurements are not always accurate and should be confirmed with multiple replicates.

While tree climbers are used in some circumstances to measure tree height, it is generally unnecessary to employ such a method in southern African woodlands, where most trees are not greater than 15 m tall.

Trigonometric methods are commonly used to estimate tree height, but all rely on the assumption that the tallest part of the tree is directly above the main trunk. While this is mostly true, many trees in highly disturbed miombo woodlands do not have a conventional shape, due to branch loss, fire damage etc. 

A clinometer estimates the angle from the researcher to the top of the tree ($\theta$). In combination with a measure of the distance from the researcher to the tree ($y$), the tree height ($H$) can be estimated: 

\begin{equation}
	H = y \tan{\theta} 
\end{equation}

Similar to the clinometer method is the ``stick'' method, which takes advantage of the principle of similar triangles, where the ratio of side lengths between two triangles with identical angles will be the same for all sides. There are multiple ways to implement the stick method, one of which is detailed in \autoref{height_stick} and \autoref{height_stick_eq}.

\begin{figure}[H]
\centering
	\includegraphics[width=\textwidth]{height_stick.drawio}
	\caption{Estimating tree height using the principle of similar triangles. Ideally this method involves three researchers. Persons A and B position themselves so person A can line up the top of the stick held by person B (thick double line) and the top of the tree in their eyeline (dashed line, i.e. the hypotenuse). Person C, not pictured, measures the distance from the tree to person B ($y$) and from person B to person A ($x$). Between trees, the values of $b$ (length of stick) and $a$ (height to person A's eye) remain constant.}
	\label{height_stick}
\end{figure}

\begin{equation}
	\label{height_stick_eq}
	H = (b-a) \times \frac{y}{x} + a
\end{equation}

\subsection{Bole height}

The section of trunk from the base of the tree to the first branch off the main trunk is known as the bole. Bole height is mostly used when estimating harvestable timber from a forest \citep{Verwijst1999}. The bole is where the most uniform timber is sourced, with the fewest imperfections caused by lateral branching. Additionally, bole height is used to understand disturbance from herbivore browsers, which remove low-hanging branches. 

\subsection{Canopy dimensions}

Canopy dimensions are difficult to measure accurately due to the complexity of the canopy form. Most often projected canopy area is modelled as an ellipse from two perpendicular measurements with tape measures from the ground, with two researchers walking out to the maximum extent of the canopy in both perpendicular directions \autoref{diam}. These perpendicular directions are often aligned to the plot edges in rectangular plots, to ensure they are truly perpendicular. Canopy area is calculated as:

\begin{equation}
	A = \pi{} a b 
\end{equation}

where $a$ and $b$ are perpendicular canopy widths.

\begin{figure}[H]
\centering
	\includegraphics[width=\textwidth]{diam.drawio}
	\caption{Schematic diagram showing a top-down view of tree canopies, with the trunk marked in brown, demonstrating measurement of perpendicular tree canopy diameters (dashed lines). Note that for trees B and C, the maximum diameter extents do not overlap the tree trunk.} 
	\label{diam}
\end{figure}

Canopy volume of angiosperm trees is mostly modelled as an ellipsoid using perpendicular canopy dimensions ($a$ and $b$), and the canopy depth ($c$):

\begin{equation}
	V = \frac{4}{3} \pi{} a b c	
\end{equation}

\begin{framed}
{\large{\textbf{SEOSAW recommended sampling strategy}}}

Measure trunk diameter on every stem >5 cm DBH and tree height on every tree with at least one stem >5 cm DBH.

Measure perpendicular canopy diameters and canopy depth on all living trees with at least one stem >20 cm DBH. 

\end{framed}

\section{Woodland canopy traits}

Woodland canopy traits can be used to understand the understorey light environment, tree-tree competition, estimate productivity, and are useful to ground-truth remotely sensed data such as airborne LiDAR or tree-cover data products. Woodland canopy traits are collected at the scale of the woodland landscape, rather than the individual tree level. 

\rowcolors{2}{gray!25}{white}
\begin{table}[H]
\centering
\caption{Common woodland canopy trait metrics.}
\begin{tabular}{rcp{11cm}}
\rowcolor{gray!50}
\hline
Metric & Unit & Description \\
\hline
Gap fraction & \% & Proportional coverage of plant canopy material as viewed from a single point with some given angular field of view. Canopy closure = 1 - gap fraction.\\
Canopy cover & \% & Proportional coverage of plant canopy material per unit ground area covered.\\
Leaf Area Index & m\textsuperscript{2} m\textsuperscript{-2} & Single-sided area of leaf (LAI) per unit ground area .\\
\hline
\end{tabular}
\end{table}

\begin{figure}[H]
\centering
	\includegraphics[width=\textwidth]{closure.drawio.pdf}
	\caption{Schematic cross section of forest demonstrating the theoretical difference between gap fraction (a) and canopy cover (b). Gap fraction is the percentage of the sky hemisphere not covered by plant material, as viewed from a single point, while canopy cover is the percentage ground area directly below the canopy.}
	\label{closure}
\end{figure}

The difference between gap fraction and canopy cover is subtle and the choice to measure either is down to the particular research questions being asked and the spatial scale of those the experimental unit. Gap fraction is preferred when paired with small scale measurements of the understorey vegetation, where the light environment at a particular point is of value, while canopy cover is preferred when paired with landscape-scale measurements of tree species composition or aboveground biomass, for example.

\subsection{Gap fraction}

Canopy gap fraction is most commonly measured using fisheye photography equipment to capture fully hemispherical photos. More recently, Terrestrial Laser Scanning (TLS) has emerged as a more precise and accurate method, but its use is still experimental \citep{Calders2020}. Due to the nature of the measurement, which estimates canopy openness within the sky hemisphere, a conventional camera with a planar lens surface is not appropriate. Many other publications have covered the various merits and idiosyncrasies of different systems for estimating gap fraction, and there is no accepted best practice \citep{Macfarlane2014, Brusa2014, Jonckheere2005, Paletto2009, Seidel2011}. Hemispherical photos should be taken using a full-frame camera, as a crop-frame camera will exclude parts of the circular image.

Briefly, here are some notes on best practice for taking hemispherical photographs in open woodlands:

\begin{itemize}
	\item{Photos should be taken pointing straight up towards the canopy, using a tripod and the aid plumb-bob or a spirit level attached to the camera.} 
	\item{Photos should be exposed so the differentiation of plant material and sky is maximised.}
	\item{Images should never be taken under full sunlight, as there is a high risk of lens flare. Images taken at dawn or dusk are preferred, but a uniformly overcast sky is also acceptable.} 
	\item{Multiple photos should be taken across a plot to estimate the mean canopy gap fraction.} 
	\item{It is common to restrict the angular field of view for gap fraction measurements to 60\si{\degree}. It can be supposed that below 60\si{\degree}, in most woodland canopies, variation in tree canopy density does little to affect sunlight penetration, due to the greater depth of canopy at these angles. Additionally, for many fisheye lenses, angles below 60\si{\degree} introduce significant visual artefacts and distortion, which may lead to biased results \citep{Jupp2008}.}
	\item{Photos should be orientated north for consistency.}
\end{itemize}

Gap fraction from hemispherical photos is most commonly measured using ImageJ, a free image analysis software \citep{Schneider2012}. 
	
\subsection{Canopy cover}

Canopy cover is much easier to estimate than gap fraction, as it doesn't require sampling the entire sky hemisphere, only the canopy directly above.

A cheap and effective method of estimating woodland canopy cover is with a periscope mirror densitometer. This apparatus is robust and comparatively cheap compared to hemispherical photography equipment, and proven to be more accurate and less prone to researcher bias than traditional convex mirror densitometers, which have now become virtually obsolete \citep{Seidel2011}. A periscope densitometer uses a cross-hair to determine in a true-false fashion whether the sky is obscured by canopy material at each sample point. \citet{GRS} make a periscope densitometer with integral spirit levels to achieve the correct view angle, for about \$100USD. The binomial distribution of true-false points is used to estimate canopy cover ($C$) simply by the ratio of ``canopy'' to ``sky'' points:

\begin{equation}
	C = \frac{\text{canopy}}{\text{canopy} + \text{sky}}
\end{equation}



We recommend a sampling density for a periscope densitometer of at least one point per 25 m\textsuperscript{2}. Thus, a 1 ha plot (10,000 m\textsuperscript{2}) would have 400 point samples. An evenly spaced grid of points is logical to provide a representative sample of the plot area. In a circular plot it is tempting to use transects of points passing through middle of the plot, but this would result in over-sampling of the plot centre and should be avoided. A triangle or square transect through the plot may be more appropriate \citep{Stumpf1993}.

Digital Cover Photography (DCP) uses a conventional compact camera or a phone camera with a planar lens surface and can also be used to estimate canopy cover. \citet{Macfarlane2007} recommend choosing a lens-camera combination which produces an angular field of view across the diagonal of the image (FOV) of \textapprox{}30\si{\degree}. FOV can be calculated as:

\begin{equation}
	\text{FOV} = 2\arctan\frac{w}{2f}
\end{equation}

Where $w$ is the camera sensor width across the diagonal, and $f$ is the focal length of the lens, both in mm. This information is normally provided by the manufacturer.

The percentage of pixels covered by vegetation in each photo is used as a measure of canopy cover. Follow the same guidelines on taking informative photos as provided above for measuring gap fraction, taking care not to cause lens flare or over-exposure. Sampling density can be significantly less than with a periscope densitometer. We recommend one photo per 100 m\textsuperscript{2}. 

\subsection{Leaf Area Index}

Leaf Area Index (LAI), the leaf area per unit ground area, is generally measured indirectly via hemispherical photography. Direct methods for estimating LAI are destructive and incredibly laborious. Inversion of the Beer-Lambert Law allows estimation of the single-sided leaf area per unit ground area using gap fraction measured at different zenith angles in the hemispherical photograph \citep{Chen1991, Nilson1971}. The basic model assumes a random leaf orientation, which is rarely true in real woodland canopies, so a correction factor is often applied, known as the ``clumping index''. There is an exhaustive literature on the derivation of LAI from hemispherical photographs and other indirect methods, recently reviewed in \citet{Yan2019}. \citet{Steege2018} provides HemiPhot, a set of R scripts constituting a modern re-write of WinPhot for estimating LAI from hemispherical photographs. 

It should be noted that LAI estimated from a photo of the woodland tree canopy will provide an over-estimate of the true LAI, owing to the non-leaf woody material in the frame. To estimate the true LAI, take two photos with the same orientation and field of view, one when leaves are not present and one with leaves present. Of course, this is not always possible in evergreen woodlands. For comparison between sites, when photos were taken during the same period of the growth season, estimating LAI with the woody material included is normally adequate.

\begin{framed}
{\large{\textbf{SEOSAW recommended sampling strategy}}}

Estimation of plot-level canopy cover using a periscope densitometer in an evenly spaced grid at a density of one measurement per 25 m\textsuperscript{2}. 
\end{framed}

\begin{landscape}
\section{Minimal equipment list}

To accomplish the key measurements outlined above in a standard woodland plot, we recommend the following field equipment. Each piece of equipment is marked according to the measurement it is used for. Note that some pieces of equipment must be present in the lab to complete measurements.

\begin{table}[H]
\begin{tabular}{r|>{\centering\arraybackslash}m{2cm}>{\centering\arraybackslash}m{2cm}>{\centering\arraybackslash}m{2cm}>{\centering\arraybackslash}m{2cm}>{\centering\arraybackslash}m{2cm}>{\centering\arraybackslash}m{2cm}>{\centering\arraybackslash}m{2cm}>{\centering\arraybackslash}m{2cm}>{\centering\arraybackslash}m{2cm}}
    \rowcolor{gray!50}
	\hline
	Item & Trunk diameter & Tree dimensions & Specific leaf area & Leaf thickness & Leaf chemistry & Wood density & Bark thickness & Canopy cover \\
	\hline
	Notebook                 & \C & \C & \C & \C & \C & \C & \C & \C \\
	Pencil, permanent marker & \C & \C & \C & \C & \C & \C & \C & \C \\
	GPS                      & \C & \C & \C & \C & \C & \C & \C & \C \\
	Permanent marker         &    &    & \C & \C & \C & \C & \C &    \\
	Clippers / saw / knife   &    &    & \C & \C & \C & \C & \C &    \\
	Plastic bags             &    &    &    &    & \C & \C & \C &    \\
	2 m height stick         & \C & \C &    &    &    & \C & \C &    \\
	2x 50-100 m tape         & \C & \C &    &    &    &    &    & \C \\
	Herbarium press          &    &    & \C & \C &    &    &    &    \\
	Diameter tape            & \C &    &    &    &    & \C & \C &    \\
	Bark punch               &    &    &    &    &    & \C & \C &    \\
	Calipers                 &    &    & \C &    &    & \C & \C &    \\
	Micrometer               &    &    &    & \C &    &    &    &    \\
	Flatbed scanner          &    &    & \C &    &    &    &    &    \\
	Periscope densitometer   &    &    &    &    &    &    &    & \C \\
	\hline
\end{tabular}
\end{table}

\end{landscape}


\printbibliography

\end{document}

