# Generate list of countries in Africa for XLSForms
# John Godlee (johngodlee@gmail.com)
# 2020-12-26

# Packages
library(seosawr)
library(countrycode)

# Load Africa
africa <- get(data(africa))

# Create dataframe of countries
out <- data.frame(iso3 = africa$iso3,
  country = countrycode(africa$iso3, 
    origin = "iso3c", dest = "country.name"))

# Write to csv, ensure UTF-8
write.csv(out, "country.csv", row.names = FALSE, fileEncoding = "UTF-8")
