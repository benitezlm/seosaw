#!/usr/bin/env sh

drawio() {
	/Applications/draw.io.app/Contents/MacOS/./draw.io --crop -x -o $2 $1 \;
}

xls2xform --pretty_print forms/stems_species_choose/stems_species_choose.xls forms/stems_species_choose/stems_species_choose.xml

xls2xform --pretty_print forms/stems_species_cjb/stems_species_cjb.xls forms/stems_species_cjb/stems_species_cjb.xml

xls2xform --pretty_print forms/stems_recensus/stems_recensus.xls forms/stems_recensus/stems_recensus.xml

xls2xform --pretty_print forms/plots/plots.xls forms/plots/plots.xml

xls2xform --pretty_print --skip_validate forms/stems_species_dynamic/stems_species_dynamic.xls forms/stems_species_dynamic/stems_species_dynamic.xml

sed -i 's/itemset\snodeset="\.\..*/itemset nodeset="\/data\/tree_repeat[position() != current()\/..\/pos and tree_group\/species_manual != '\'\'']">/g' forms/stems_species_dynamic/stems_species_dynamic.xml

sed -i 's/ref="species_manual.*/ref="tree_group\/species_manual"\/>/g' forms/stems_species_dynamic/stems_species_dynamic.xml

mv forms/stems_species_cjb/itemsets.csv forms/stems_species_cjb/stems_species_cjb-media

drawio drawio/loc.drawio drawio/loc.pdf
drawio drawio/catenal.drawio drawio/catenal.pdf
drawio drawio/nested.drawio drawio/nested.pdf
drawio drawio/subplot.drawio drawio/subplot.pdf

pdftoppm -png drawio/catenal.pdf > drawio/catenal.png
pdftoppm -png drawio/loc.pdf > drawio/loc.png
pdftoppm -png drawio/nested.pdf > drawio/nested.png
pdftoppm -png drawio/subplot.pdf > drawio/subplot.png

gm convert -append drawio/nested.png drawio/subplot.png drawio/nested_subplot.png

cp drawio/catenal.png forms/plots/plots-media/catenal.png
cp drawio/nested.png forms/plots/plots-media/nested.png
cp drawio/subplot.png forms/plots/plots-media/subplot.png
cp drawio/loc.png forms/stems_species_choose/stems_species_choose-media/loc.png
cp drawio/nested_subplot.png forms/stems_species_choose/stems_species_choose-media/nested_subplot.png
cp drawio/loc.png forms/stems_species_cjb/stems_species_cjb-media/loc.png
cp drawio/nested_subplot.png forms/stems_species_cjb/stems_species_cjb-media/nested_subplot.png
cp drawio/loc.png forms/stems_recensus/stems_recensus-media/loc.png
cp drawio/nested_subplot.png forms/stems_recensus/stems_recensus-media/nested_subplot.png
cp drawio/loc.png forms/stems_species_dynamic/stems_species_dynamic-media/loc.png
cp drawio/nested_subplot.png forms/stems_species_dynamic/stems_species_dynamic-media/nested_subplot.png

rm drawio/*.pdf drawio/*.png

