This is a variant of the stem data entry form which allows the user to supply their own CSV file of species names rather than using the dynamically populating list method in the other stem data entry form. `species.csv` should be a CSV file formatted like so:

| list_name | name | label |
|-|-|-|
| opt_species | Julbernardia globiflora | Mtondoro |
| opt_species | Afzelia quanzensis | Mkongo |
| opt_species | Brachystegia spiciformis | Myombo |
| ... | ... | ... |

Where the first column must contain `opt_species` for each row, the second column contains the latin binomial name of the species, and the third column contains the name of the species as you would like it to appear to the user. 

It is wise to add a row in `species.csv` where the `label` is `Other` and the `name` is `Other`. This will unlock a question below where the user can manually enter the name of the species.

`species.csv` will need to be manually added to each individual deployment of this form as an asset. Methods to achieve this depend on the deployment platform, e.g. KoboToolbox, ODK Central, ODK Briefcase.
