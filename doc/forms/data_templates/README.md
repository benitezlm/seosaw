# Data entry templates for SEOSAW plot and stem data

Each file contains a table of columns identical to those in the SEOSAW dataset column specification, with examples of possible values for each column.

Note that there is one line of metadata at the top of these files which may need to be deleted to ensure compatability with whatever spreadsheet program you are using. The metadata denotes version of the file.
